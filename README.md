**Jeu de rôle**
 
Bienvenue au jeu de rôle créé par Théophile Yvars et Daniel Hilari. Ce jeu est calqué sur un livre ou vous êtes le héros. Pour ce jeu, nous nous sommes inspirés de _Super Seducer_. Vous êtes un jeune garçon en seconde qui veut séduire une belle fille de seconde dont le nom est Charlotte. Pour cela vous devez effectuer les meilleurs choix possibles pour réussir. 
 
**Installation**
 
Téléchargez l'ensemble du dossier JeuGenieLogiciel.zip ou autre format. Tout décompresser et utiliser le makefile pour compiler. Pour l'utiliser faite dans le terminal Ubuntu et dans le dossier /jeugenielogiciel-master la commande : make 

Lorsque le jeu sera compilé dans le dossier /jeugenielogiciel-master/obj/ exécuter la classe classeMain avec la commande : java classeMain

Bon amusement ! 
 
**Gameplay**
 
Vous serez amené à effectuer des choix pour avancer dans l'histoire. Faites attention dans les trois choix proposés vous pourrez perdre, être amener à une autre situation avec d'autres choix, être amener à un affrontement ou une épreuve plus ou moins ou pendu. 
Pour effectuer vos choix dans une situation basique vous devez écrire dans le terminal 0,1 ou 2 pour communiquer votre choix. 

Dans un affrontement, de type Shifumi, vous devez faire le choix d'une des trois attaques possibles. Également utiliser 0,1 ou 2. Le choix numéro 1 bat le choix numéro 2, le 2 bat le 3 et le 3 bat le 1. Bonne chance à vous ! 

Dans une épreuve de plus ou moins, vous devez trouver un nombre. Pour cela vous serez guidé par le jeu qui vous dira si le nombre lu par le terminal est supérieur ou inférieur au nombre cherché. 

Dans l'épreuve, vous devez trouver le mot demandé. Pour cela vous pouvez essayer des lettres ou si vous êtes sûr de vous le mot en entier. Indiquez le choix de lire une lettre ou le mot en entier avec 0 ou 1 et communiquez votre proposition.
 
Bonne chance ! 
 
**Alert Spoiler**
 
Après avoir essayer le jeu, si vous voulez savoir tous les secrets du jeu : Ou gagner des objets, quel est la fin qui vous plaît, les chemins où l'on perd et caetera veuillez trouver l'arbre de l'histoire du jeu dans ce lien : 
 
https://drive.google.com/file/d/1bzZbuZgg65FlwN529YWinvQ-hpFr0iVe/view?usp=sharing
 
 

