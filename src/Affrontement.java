import java.util.*;


public class Affrontement extends Scene{

/*ATTRIBUTS*/
	short VieAdversaire; 
	String Element_un; 
	String Element_deux;
	String Element_trois;
	short Manche;

	/*  un > deux > trois > un */
	/* pierre > ciseaux > feuille > pierre */


	/***************************************************************************************************
										  __  __      _   _           
										 |  \/  |    | | (_)          
										 | \  / | ___| |_ _  ___ _ __ 
										 | |\/| |/ _ \ __| |/ _ \ '__|
										 | |  | |  __/ |_| |  __/ |   
										 |_|  |_|\___|\__|_|\___|_|   
                              
	***************************************************************************************************/



/*Constructeur*/
	Affrontement(int vie, String element1, String element2, String element3,int id){
		/* Initialise un affrontement de type shifumi */
		this.VieAdversaire = (short)vie;
		this.Element_un = element1;
		this.Element_deux = element2;
		this.Element_trois = element3;
		this.Manche = (short)0;
		this.ID = id;
	}

/*--------------------------------------------------------------------------------------------------*/
/*METHODES*/


	
	
	boolean erreurChoix(short lechoix){
        /** Exeption de lecture de choix, lorsqu'il y a une erreur*/
		if((short)lechoix < (short)0 || (short)lechoix > (short)2)
		{
			/* Le joueur n'as pas bien communiquer son choix*/
			System.out.print("\n");
			System.out.println("\t Aieeee... vous cherchez une attaque inconnue...");
			System.out.println("\t Votre Adversaire vous a pris par surprise pendant votre recherche...");
			//perdreVie();
			return true;
		}
		return false;

	}
	
	
	
	short choixIA(){
		/** Le choix de l'IA, aleatoire*/
		short choixIA = (short)(Math.random()*(3-0));
		if((short)choixIA == (short)0)
			System.out.println(this.Element_un);
		if((short)choixIA == (short)1)
			System.out.println( this.Element_deux);
		if((short)choixIA == (short)2)
			System.out.println(this.Element_trois);		
		return choixIA;
	}

	short comparaisonsElement(short joueur, short ia){
		/** compare le choix de l'ordianteur et du joueur pour savoir qui gagne 0 win joueur 1 win ia 2 egalité*/
		/* 2 : il y a egalité ; 0 l'ia gagne ; 1 le joeur gagne ; -1 erreur*/
		if((short)joueur < (short)0 || (short)joueur > (short)2)
			return -1;
		if((short)joueur == ia)
			return (short)2;
		if((short)joueur < (short)2 && ia < (short)2)
		{
			if((short)joueur > ia )
				return (short)1; 
			return (short)0; 
		}
		if((short)joueur == (short)0)
			return (short)1;

		if((short)joueur == (short)1)
			return (short)0;

		if((short)ia == (short)0)
			return (short)0;

		if((short)ia == (short)1)
			return (short)1;
		return -1;
	}

	void perdreVie(Personnage heros){
		/** Fait perdre de la vie au joueur au cas ou il perd une manche*/
		heros.vie = (short)(heros.vie -20);
	}

	void perdreVieIA(){
		/** Fait perdre de la vie au personnage incarné par l'IA */
		this.VieAdversaire = (short)(this.VieAdversaire -20);
	}

	boolean verificationFin(Personnage heros){
		/** Verifie si un des combattant n'as plus de vie*/
		if(heros.vie <= 0 || this.VieAdversaire <= 0)
			return false;
		return true;
	}
	
	boolean gagnant(Personnage heros){
		/* Defini le gagnant de la partie d'affrontement */
		if(heros.vie <= 0)
		{
			System.out.println("\t Gagne l'ia");
			return true;
		}	
		if(this.VieAdversaire <= 0)
			System.out.println("\t Gagne le joueur");
		return false; 
	}

	int play(Personnage heros){
		/* Execution d'une partie d'affrontement */
		int idSuivant = this.idSceneSuivante1;
		System.out.print("\n\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n");
        /** Commence l'affrontement*/ 
		System.out.println("\t C'est l'heure de l' AFFRONTEMEEEEENT !\n\n");
		
		int chanceGagne = 0;
		
		Equipement equi = null;
		Equipement recup = null;
		/* element uniques a chaque affrontement pour les equipement presents */
		if(Inventaire.rechercheId(10, classeMain.getHero()) && (this.ID == 27 || this.ID == 33 )) {
			recup = (Equipement) heros.inventaire.sac.get(Inventaire.actionObjetOutil(10));
			heros.vie += recup.valeurArmure;
			System.out.print("\nVotre niveau de confiance en soi : " + heros.vie);
		}
		if(Inventaire.rechercheId(11, classeMain.getHero()) && (this.ID == 27 || this.ID == 33 )) {
			recup = (Equipement) heros.inventaire.sac.get(Inventaire.actionObjetOutil(11));
			heros.vie += recup.valeurArmure;
			System.out.print("\nVotre niveau de confiance en soi : " + heros.vie);
		}
		if(Inventaire.rechercheId(20, classeMain.getHero()) && (this.ID == 27 || this.ID == 33 )) {
			recup = (Equipement) heros.inventaire.sac.get(Inventaire.actionObjetOutil(20));
			chanceGagne = recup.valeurAttaque;
			System.out.print("\nPourcentage de réussite : " + chanceGagne);
		}
		
		if(this.ID == 22) {
			classeMain.affichageCopain();
		}
		/* Affichage */
		System.out.print("\n\n" + this.descrition + "\n\n");
		
		//this.affichageAffrontement();

		while(this.verificationFin(heros) == true)
		{
			this.affichageAffrontement();
			System.out.println("\t Vie restante : " + heros.vie + " -- Vie adversaire : " + this.VieAdversaire);
			short remporte = (short)-1;
			remporte = this.comparaisonsElement(this.choixJoueur(),this.choixIA());

			if((short)remporte == (short)0)
			{
				perdreVieIA();
			}
			if((short)remporte == (short)1 || (short)remporte == (short)-1)
			{
				if(chanceGagne > 0 && (this.ID == 27 || this.ID == 33 )) {
					if(probaGagne(chanceGagne)){
						System.out.print("\nVotre réthorique vous a permis de gagner assurément cette partie.\nMême si vous deviez perdre ..\n\n");
						perdreVieIA();
					}else {
						perdreVie(heros);
					}
				}else {
					perdreVie(heros);
				}
			}
			this.Manche++;
			System.out.print("\n\n**-----------------------------------------------------------------------------------------------**\n\n");
		}
		
		boolean winner = gagnant(heros);
		
		System.out.print("\n\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n");
		
		if (winner ==  false)
		{
			Personnage.reinisialiseVie();
			if(this.ID == 14){
				equi = new Equipement(3, "Fleur du séducteur", 0, 0);
				classeMain.afficheFleur();
				System.out.print("Vous avez gagné : " + equi.bonusObjet + ".\nDonne l'accés à une voie plus rapide pour séduire Charlotte\n");
				Inventaire.addObjetInventaire(equi);
			}
			if(Inventaire.rechercheId(1, classeMain.getHero()) == false && (this.ID == 27 || this.ID == 33)) {
	            idSuivant = 24;
	            this.VieAdversaire = 100;
	            System.out.println("Ah, mais il vous manque la clef de chez vous ... \nRetour à la maison et n'oublié pas de prendre votre clef\n\n");
	        }
			if(Inventaire.rechercheId(1, classeMain.getHero()) == true && (this.ID == 27 || this.ID == 33)) {
				System.out.print("\n\n" + heros.prenom + " était stupéfait, Charlotte était vraiment douée à fifa. Une fille qui jouait à ses \n" + 
						"jeux vidéos préférés et qui était en plus doué ça ne pouvait qu’être la femme de sa vie. Le \n" + 
						"stress montait. Il fallait gagner !! Le dernier point qui pourrait tout changer..., et voilà c’est \n" + 
						"bon il a gagné !! Quel soulagement, il la regarda, elle était belle à bouder par sa perte. Il se \n" + 
						"pencha pour l’embrasser, elle lui rendit heureuse d’avoir trouvé son prince charmant. Et c’est \n" + 
						"ainsi, qu’ils vécurent heureux et eurent beaucoup d’enfants. \n" + 
						"\n\n");
				
				System.out.print("                                            |\":::\\\n" + 
						"      /```|      .@@@@@,         ,,,,       66 :::|\n" + 
						"     |` 66|_     @@@@@@@@,      @@@@@@,    (_   ':D\n" + 
						"     C     _)    aa`@@@@@@      aa @@@@,    |_,  /\n" + 
						"(\\/)  \\ ._|     (_   ?@@@@     (_  ?@@@@     \\__(\n" + 
						" \\/    ) /       =' @@@@\"       =' @@@@\"     //`\\\\     @@@@\n" + 
						"      /`\\\\        \\_(```         ) @@@\"     | | ||    ee \"@@\n" + 
						"     /| |Y|       /``\\          /-'@@@,     | |_||   (_   `D\n" + 
						"    | | |#|      / | ||        / / \\@@@     | | ||    |_  /\n" + 
						"    | | | #\\     \\ | ||        \\ | |@@@     | | ||     \\_(\n" + 
						"    | | |  #|    / | ||         || |@@\"     |-| |<     /  \\\n" + 
						"    :=| |===:   /  | | \\        || |@\"      ||(((|     | ||\n" + 
						"    | |_|, /    |  |_|  \\       ||=|\\       ||_:_/     | ||\n" + 
						"     \\)))||     |  (((   |      |((( \\       || |      | ||\n" + 
						"  |~~~`-`~~~|    \\~~~~~~~|      |____/       || |      |_||\n" + 
						"  |         |     \\     /        || /        (( |      | `W\n" + 
						"  |_________|      ((  |         (( |        || |      || |\n" + 
						"  |_________|      ||  |         || |        || |      || |\n" + 
						"      | ||         ||  |         || |        ||=|      || |\n" + 
						"      |_||__       ||__|         //^)      __||_|     _||_|\n" + 
						" jgs  (____))    ((____)        ((_/Y     ((_____)   ((____)\n" + 
						"\n\n");
			}
			return idSuivant;
		}
		System.out.print("\n\n   _____          __  __ ______        ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|      / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__        | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|       | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____      | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|      \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                                   \n" + 
				"                                                                   \n\n");
		return Scene.IdMax;
	}
	
	boolean probaGagne(int val) {
		boolean reponse = false;
		
		int gagne = (int)(Math.random()*(101-0));
		
		//System.out.print("\n"+gagne+" et "+val+"\n");
		
		if(gagne <= val) {
			reponse = true;
		}
		
		return reponse;
	}

	/***************************************************************************************************
								  _____  _       _                        
								 |  __ \(_)     | |                       
								 | |  | |_  __ _| | ___   __ _ _   _  ___ 
								 | |  | | |/ _` | |/ _ \ / _` | | | |/ _ \
								 | |__| | | (_| | | (_) | (_| | |_| |  __/
								 |_____/|_|\__,_|_|\___/ \__, |\__,_|\___|
								                          __/ |           
								                         |___/            

	 ***************************************************************************************************/

void affichageAffrontement(){
        /** Affiche le debut de l'affrontement */
		int taille = this.Element_un.length() + this.Element_deux.length() + this.Element_trois.length()+ 18;
		System.out.println("\t Voici les choix possible pour l'affrontement : ");
		System.out.print("\t ");
		for(int i = 0; i<taille; i++)
		{
			System.out.print("-");
		}
		System.out.print("\n");
		System.out.println("\t |"+ this.Element_un+" (0)| "+ this.Element_deux+" (1)| "+ this.Element_trois+ " (2)|");
		System.out.print("\t ");
		for(int i = 0; i<taille; i++)
		{
			System.out.print("-");
		}
		System.out.print("\n");
		System.out.print("\n\n");
	}

	/***************************************************************************************************

									   _____            _        //\  _      
									  / ____|          | |      |/ \|| |     
									 | |     ___  _ __ | |_ _ __ ___ | | ___ 
									 | |    / _ \| '_ \| __| '__/ _ \| |/ _ \
									 | |___| (_) | | | | |_| | | (_) | |  __/
									  \_____\___/|_| |_|\__|_|  \___/|_|\___|
                                                                             

	 ***************************************************************************************************/

short choixJoueur(){
		/** Le choix d'attaque du joueur se decide ici et maintenant*/
		System.out.print("\n\n");
		System.out.println("\t ---------------------");
		System.out.println("\t | Manche numero : "+ this.Manche +" |");
		System.out.println("\t ---------------------");
		System.out.print("\n");
		System.out.println("\t Veuillez choisir votre attaque !! ");
		System.out.print("\t Choix numero : ");
		short choix; 
		Scanner scanChoix = new Scanner(System.in);
		choix = scanChoix.nextShort();
		
		System.out.print("\n");
		if((short)choix == (short)0)
		{
			System.out.print("\t "+ this.Element_un+" VS ");
		}
		if((short)choix == (short)1)
		{
			System.out.print("\t "+ this.Element_deux+" VS ");
		}
		if((short)choix == (short)2)
		{
			System.out.print("\t "+ this.Element_trois+" VS ");
		}
		if(erreurChoix(choix) == true)
			return (short)-1; 
		return choix;
	}
}
