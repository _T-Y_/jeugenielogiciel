import java.io.IOException;
import java.util.Scanner;

public class ParcoursSimple extends Scene{
	
	public String descritionCheminPossible;
	public int cheminInvalide;
	
	/***************************************************************************************************
										  __  __      _   _           
										 |  \/  |    | | (_)          
										 | \  / | ___| |_ _  ___ _ __ 
										 | |\/| |/ _ \ __| |/ _ \ '__|
										 | |  | |  __/ |_| |  __/ |   
										 |_|  |_|\___|\__|_|\___|_|   

	 ***************************************************************************************************/
	
	ParcoursSimple(int ID){
		this.ID = ID;
	}
	
	/*
	 * Fonction qui permet d'executer cette classe
	 */
	int play(Personnage hero, Memoire mem) throws IOException, InterruptedException {
		int sceneSuivante = 0;
		int rep = 0;
		
		this.afficheDescription(); // Affichage du descriptif de la scene
		
		if(Inventaire.rechercheId(1,hero)) {
			afficheClef(); // Si clef presente, on affiche qu'on a une clef
		}
		
		this.afficherCheminPossible(); // Montre à l'utilisatueur les chemins possible
		
		rep = this.cheminChoisi(); // recup le choix de l'utilisatieur
		
		if(this.ID == 24 && rep == 4) { // Si On est dans la scene 24, soit la scene de la maison, on peut recup la clef en mettant "4"
			if(Inventaire.rechercheId(1, hero)) { // Si clef deja presente, inutile de la mettre en double
				System.out.print("\nClef deja presente\n");
			}else { // Sinon, creation de la clef, et stockage dans l'invenatire du joueur
				Clef clef = new Clef(1, "Clef de la victoire");
				Inventaire.addObjetInventaire(clef);
				classeMain.afficheClef(); // Affiche le ascii art
				System.out.print("\n\nOh, vous avez trouvé une clef !!\n\n");
				Thread.sleep(2000);
			}
			/*
			 * Aprés avoir recup la clef, la scene suivante est la même, car nous n'avons
			 * toujours pas décidé ou nous allons ici
			 */
			sceneSuivante = this.ID;
		}else {
			/*
			 * En fonction du choix de l'utilisation, cette méthodes va cherche l'id correspondant
			 */
			sceneSuivante = this.convertChoixEnIdScene(rep,mem,hero);
		}

		return sceneSuivante;
	}
	
	/*
	 * Méthode qui cherche l'id de la scene suivante en fonction du choix de l'utilisateur
	 */
	int convertChoixEnIdScene(int num, Memoire mem, Personnage hero) throws IOException {
		int convert = this.ID;
		int rep;

		if(num == 0)
			convert = this.idSceneSuivante1;
		else if (num == 1)
			convert = this.idSceneSuivante2;
		else if (num == 2)
			convert = this.idSceneSuivante3;
		else {
			rep = Menu.afficheMenu(this.ID, hero, mem);
			if(rep == 1)
				convert = Scene.IdMax;
			if(rep == 2)
				convert = classeMain.indiceScene; 
		}
		
		return convert;
	}
	
	/***************************************************************************************************
									  _____  _       _                        
									 |  __ \(_)     | |                       
									 | |  | |_  __ _| | ___   __ _ _   _  ___ 
									 | |  | | |/ _` | |/ _ \ / _` | | | |/ _ \
									 | |__| | | (_| | | (_) | (_| | |_| |  __/
									 |_____/|_|\__,_|_|\___/ \__, |\__,_|\___|
									                          __/ |           
									                         |___/            

	 ***************************************************************************************************/

	void afficheDescription() {
		System.out.print(this.descrition);
	}
	
	void afficheClef() {
		System.out.print("\nVous avez une clef\n");
	}
	
	void afficherCheminPossible() {
		//System.out.print(this.descritionCheminPossible);
		if(Inventaire.rechercheId(3, classeMain.getHero()) == false && this.ID == 24) {
			this.idSceneSuivante3 = 26;
			this.SceneSuivante3 = "Il vous manque une fleur pour aller ici .. Travailler?";
		}
		
		if(Inventaire.rechercheId(1, classeMain.getHero()) == false && this.ID == 27) {
			this.idSceneSuivante1 = 24;
			this.SceneSuivante1 = "Ah, mais il vous manque la clef de chez vous ... \nRetour à la maison et n'oublié pas de prendre votre clef\n\n";
		}
		
		System.out.print("\n__________________________________________________________________________________________________________________________\n\n " + this.SceneSuivante1 + "(0) | " + this.SceneSuivante2 + "(1) | " + this.SceneSuivante3 + "(2) | MENU(3) \n__________________________________________________________________________________________________________________________\n\n");
	}
	
	/***************************************************************************************************
							
								   _____            _        //\  _      
								  / ____|          | |      |/ \|| |     
								 | |     ___  _ __ | |_ _ __ ___ | | ___ 
								 | |    / _ \| '_ \| __| '__/ _ \| |/ _ \
								 | |___| (_) | | | | |_| | | (_) | |  __/
								  \_____\___/|_| |_|\__|_|  \___/|_|\___|
                                       

	 ***************************************************************************************************/

	int cheminChoisi() {
		int choix = 0;
		
		System.out.print("\nVotre choix : ");
		
		Scanner sc = new Scanner(System.in);
		choix = sc.nextInt();
		
		if(this.ID != 24) {
			if(choix <= 3 && choix >= 0)
				return choix;
		}else {
			if(choix <= 4 && choix >= 0)
				return choix;
		}
		
		System.out.print("Mauvaise saisie\n");
		return cheminChoisi();
	}

}