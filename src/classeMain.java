/*
  
 __     __                     _______ _       __             _     _ _      
 \ \   / /                    |__   __| |     /_/            | |   (_) |     
  \ \_/ /_   ____ _ _ __ ___     | |  | |__   ___  ___  _ __ | |__  _| | ___ 
   \   /\ \ / / _` | '__/ __|    | |  | '_ \ / _ \/ _ \| '_ \| '_ \| | |/ _ \
    | |  \ V / (_| | |  \__ \    | |  | | | |  __/ (_) | |_) | | | | | |  __/
  _ |_|_ _\_/ \__,_|_|  |___/____|_|  |_| |_|\___|\___/| .__/|_| |_|_|_|\___|
 | |  | (_) |          (_) |  __ \            (_)    | | |                   
 | |__| |_| | __ _ _ __ _  | |  | | __ _ _ __  _  ___| |_|                   
 |  __  | | |/ _` | '__| | | |  | |/ _` | '_ \| |/ _ \ |                     
 | |  | | | | (_| | |  | | | |__| | (_| | | | | |  __/ |                     
 |_|  |_|_|_|\__,_|_|  |_| |_____/ \__,_|_| |_|_|\___|_|                     
                                                                  
 */

import java.io.IOException;

public class classeMain {
	
	private static Personnage hero;
	
	static Scene jeu[];
	static int indiceScene;
	static int fin;
	
	public static void main (String[] args) throws IOException, InterruptedException{
		
		System.out.print("\n\n**************************************************************\n\n");
		System.out.print("  ____  _                                 \n" + 
				" |  _ \\(_)                                \n" + 
				" | |_) |_  ___ _ ____   _____ _ __  _   _ \n" + 
				" |  _ <| |/ _ \\ '_ \\ \\ / / _ \\ '_ \\| | | |\n" + 
				" | |_) | |  __/ | | \\ V /  __/ | | | |_| |\n" + 
				" |____/|_|\\___|_| |_|\\_/ \\___|_| |_|\\__,_|\n" + 
				"                                          \n" + 
				"                                          ");
		System.out.print("\n\n**************************************************************");
		Thread.sleep(1000);
		System.out.print("\n\nLa fille dessous c'est ... \n\n");
		Thread.sleep(1000);
		classeMain.affichageCharlotte();
		Thread.sleep(1000);
		System.out.print("\n                                                    \n" + 
				"                                                    \n" + 
				" ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ \n" + 
				"▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌\n" + 
				" ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ \n" + 
				"                                                    \n" + 
				"                                                    \n");
		Thread.sleep(1000);
		/***************************************************************************************************
						  _____       _ _   _       _ _           _   _             
						 |_   _|     (_) | (_)     | (_)         | | (_)            
						   | |  _ __  _| |_ _  __ _| |_ ___  __ _| |_ _  ___  _ __  
						   | | | '_ \| | __| |/ _` | | / __|/ _` | __| |/ _ \| '_ \ 
						  _| |_| | | | | |_| | (_| | | \__ \ (_| | |_| | (_) | | | |
						 |_____|_| |_|_|\__|_|\__,_|_|_|___/\__,_|\__|_|\___/|_| |_|
		                                                            
		                                                            
	 	***************************************************************************************************/
		
		classeMain.initIndiceScene(0);
		fin = 0;
		
		Initialisation jeuInit;
		jeuInit = new Initialisation();
		classeMain.initJeu(jeuInit);
		classeMain.initScript(jeuInit);
		
		Memoire mem = new Memoire();
		//Runtime.getRuntime().exec("clear");
		
		Thread.sleep(2000);
		hero = classeMain.getHero();
		System.out.print("\n\nLe gas dessous c'est toi, " + hero.prenom + "...\n\n");
		Thread.sleep(2000);
		classeMain.affichageHero();
		Thread.sleep(2000);
		System.out.print("\n                                                    \n" + 
				" ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ \n" + 
				"▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌\n" + 
				" ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ \n" + 
				"                                                    \n" + 
				"                                                    \n");
		
		/****************************************************************************************************
									 			_            
										       | |           
										       | | ___ _   _ 
										   _   | |/ _ \ | | |
										  | |__| |  __/ |_| |
										   \____/ \___|\__,_|
	   
		 ***************************************************************************************************/

		/*
		 * La méthode static actionScene() permet de naviguer dans le tableau de scene et d'executer
		 * la méthode play en fonction du type de la scene.
		 * Parcours Simple, affrontement, epreuve ...
		 * Elles renvoient le numéro de la scene suivante en fonction des choix faits
		 */
		
		while(fin == 0) {
			indiceScene = Scene.actionScene(jeu[indiceScene], getHero(), mem);
			
			if(indiceScene == Scene.IdMax) {
				fin = 1;
				System.out.print("\n\n**************************************************************\n\n");
				System.out.print("  ______ _             _              _            \n" + 
						" |  ____(_)           | |            | |           \n" + 
						" | |__   _ _ __     __| |_   _       | | ___ _   _ \n" + 
						" |  __| | | '_ \\   / _` | | | |  _   | |/ _ \\ | | |\n" + 
						" | |    | | | | | | (_| | |_| | | |__| |  __/ |_| |\n" + 
						" |_|    |_|_| |_|  \\__,_|\\__,_|  \\____/ \\___|\\__,_|\n" + 
						"                                                   \n" + 
						"                                                   ");
				System.out.print("\n\n**************************************************************\n\n");
			}
		}
		
	 }
	
	/*
	 * Quelques affichages pour le fun ..
	 */
	
	public static void afficheFleur() {
		System.out.print("\n        .-~~-.--.\n" + 
				"       :         )\n" + 
				" .~ ~ -.\\       /.- ~~ .\n" + 
				" >       `.   .'       <\n" + 
				"(         .- -.         )\n" + 
				" `- -.-~  `- -'  ~-.- -'\n" + 
				"   (        :        )           _ _ .-:\n" + 
				"    ~--.    :    .--~        .-~  .-~  }        \n" + 
				"        ~-.-^-.-~ \\_      .~  .-~   .~\n" + 
				"                 \\ \\'     \\ '_ _ -~\n" + 
				"                  `.`.    //\n" + 
				"         . - ~ ~-.__`.`-.//\n" + 
				"     .-~   . - ~  }~ ~ ~-.~-.\n" + 
				"   .' .-~      .-~       :/~-.~-./:\n" + 
				"  /_~_ _ . - ~                 ~-.~-._\n" + 
				"                                   ~-.<\n");
	}
	
	public static void afficheClef() {
		System.out.print("\n     8 8 8 8                     ,ooo.\n" + 
				"     8a8 8a8                    oP   ?b\n" + 
				"    d888a888zzzzzzzzzzzzzzzzzzzz8     8b\n" + 
				"     `\"\"^\"\"'                    ?o___oP'\n");
	}
	
	public static void afficheRetoriqueDeLAmour() {
		System.out.print("\n               z$6*#\"\"\"\"*c     :@$$****$$$$L\n" + 
				"                  .@$F          \"N..$F         '*$$\n" + 
				"                 /$F             '$P             '$$r\n" + 
				"                d$\"                                #$      '%C\"\"\"$\n" + 
				"               4$F                                  $k    ud@$ JP\n" + 
				"               M$                                   J$*Cz*#\" Md\"\n" + 
				"               MR                              'dCum#$       \"\n" + 
				"               MR                               )    $\n" + 
				"               4$                                   4$\n" + 
				"                $L                                  MF\n" + 
				"                '$                                 4$\n" + 
				"                 ?B .z@r                           $\n" + 
				"               .+(2d\"\" ?                          $~\n" + 
				"    +$c  .z4Cn*\"   \"$.                           $\n" + 
				"'#*M3$Eb*\"\"         '$c                         $\n" + 
				"   /$$RR              #b                      .R\n" + 
				"   6*\"                 ^$L                   JF\n" + 
				"                         \"$                 $\n" + 
				"                           \"b             u\"\n" + 
				"                             \"N.        xF\n" + 
				"                               '*c    zF\n" + 
				"                                  \"N@\"\n" + 
				"\n");
	}
	
	public static void afficheFifa() {
		System.out.print("\n       _...----.._\n" + 
				"     ,:':::::.     `>.\n" + 
				"   ,' |:::::;'     |:::.\n" + 
				"  /    `'::'       :::::\\\n" + 
				" /         _____     `::;\\\n" + 
				":         /:::::\\      `  :\n" + 
				"| ,.     /::SSt::\\        |\n" + 
				"|;:::.   `::::::;'        |\n" + 
				"::::::     `::;'      ,.  ;\n" + 
				" \\:::'              ,::::/\n" + 
				"  \\                 \\:::/\n" + 
				"   `.     ,:.        :;'\n" + 
				"     `-.::::::..  _.''\n" + 
				"        ```----'''\n");
	}
	
	public static void afficheArmure() {
		System.out.print("\n _________________________ \n" + 
				"|<><><>     |  |    <><><>|\n" + 
				"|<>         |  |        <>|\n" + 
				"|           |  |          |\n" + 
				"|  (______ <\\-/> ______)  |\n" + 
				"|  /_.-=-.\\| \" |/.-=-._\\  | \n" + 
				"|   /_    \\(o_o)/    _\\   |\n" + 
				"|    /_  /\\/ ^ \\/\\  _\\    |\n" + 
				"|      \\/ | / \\ | \\/      |\n" + 
				"|_______ /((( )))\\ _______|\n" + 
				"|      __\\ \\___/ /__      |\n" + 
				"|--- (((---'   '---))) ---|\n" + 
				"|           |  |          |\n" + 
				"|           |  |          |\n" + 
				":           |  |          :     \n" + 
				" \\<>        |  |       <>/      \n" + 
				"  \\<>       |  |      <>/       \n" + 
				"   \\<>      |  |     <>/       \n" + 
				"    `\\<>    |  |   <>/'         \n" + 
				"      `\\<>  |  |  <>/'         \n" + 
				"        `\\<>|  |<>/'         \n" + 
				"          `-.  .-`           \n" + 
				"            '--'\n");
	}
	
	public static void affichageCopain() {
		System.out.print("\n\n" + 
				"                                          `::..   .\n" + 
				"                           `?XXX.  `T{/:.   %X/!!x \"?x.\n" + 
				"                             \"4{7@( '!+!!X(:.`4!!X!x.?h7h\n" + 
				"                         `!(:. ~!!!f(~!!!+!!{{.'~+h!tX!!?hh:.\n" + 
				"                    '`X!.  !(d!X!!H!?{{``\"!:?{{!{X*!?tX!!H*))h.\n" + 
				"                  ...  '!X(!X!{{?@f!!!{!{x.!!%!!!%!!!)@Thh!!X)!).\n" + 
				"                   ^!!!{:!(((!!: ~((({!!!h+!{{!X!+%?+{!!?!+)!+X(!+\n" + 
				"               -    `\\tXX{(~!!!!!:.!.%%(!!!!!!!!!X!))!!!!X%``%!!!(>\n" + 
				"               ^X>:x. {!!!!X: ~!!*!{!!!{!~!X!)%!{!!!)?@!!!?!)?!!!>~\n" + 
				"                 `X(!!:!!!{{(!!.)!%(:\\!!:%~!~\\!t!! `H!)~~!!!!!!(?@\n" + 
				"                  `!X: `)!!!C44XX!!!.%%.X:>-> %!!X! /!~!.'!> !S!!!\n" + 
				"              +{..  \\X%\\.'{??X!!!t!!~!!{!~!~'.!~~~ -~` {> !~ /!X`\n" + 
				"                `X!XXM!!4!%\\(4!!!!%(`,zccccd$$$$$$$$$ccx ` .~\n" + 
				"                  \"XLS@!)!!%L44X!!! d$$$$$$$$$$$$$$$$$$$,  '^\n" + 
				"                   `!X?%:!!??X!4?*';$$$$$$$$$$$$$$$$$$$$$\n" + 
				"                  `iXM:!!?Xt!XH!!! 9$$$$$$$$$$$$$$$$$$$$$\n" + 
				"                   `X3tiXS#?WH!X!! $$$$$$$$$$$$$$$$$$$$$$\n" + 
				"                   .MX?*StXX?X!!W? $$$$$$$>?$$$$$$$$$$$$\n" + 
				"                    8??M%T%' r `  ;$$$$$$$$$,?$$$$$$$$$F\n" + 
				"                    'StMX!': J$$d$$$$$$$$$$$$h ?$$$$$$\"\n" + 
				"                     tM9MH d$$$$$$$$$$$$$$C???r{$$$F,r\n" + 
				"                     4M?t':$$$$$$$$$$$$$$$$h. $$$,cP\"\n" + 
				"                     'M>.d$$$$$$$$$$$$$$$$$>d$$${.,\n" + 
				"                      ,d$$$$$$$$$$$$$$$$$'cd$$$$r\"\n" + 
				"                      `$$$$$$$$$$$$$$$??$Jcii`?$h\n" + 
				"                       $$$$$$$$$$$$$F,;;, \"?h,`$$h\n" + 
				"                      j$$$$$$$$$$$$$.CC>>>>c`\"  `\"        ..,g q,\n" + 
				"                   .'!$$$$$$$$$$$$$' `''''''            aq`?g`$.Bk\n" + 
				"               ,- '  \"?$$$$$$$$$$$$$$d$$$$$c, .         .)od$$$$$$\n" + 
				"          , -'           `\"\"'   `\"?$$$$$$$$$??=      .d$$$$$$$$F'\n" + 
				"        ,'                           `??$$P       .ed$$P\"\"   `\n" + 
				"       ,                                `.      z$$$\"\n" + 
				"       `:dbe,                          x,/    e$$F'\n" + 
				"       :$$$$P'`>                       $F  z$$$\"\n" + 
				"      d$$$P\"'  >                       $Fe$$$\"\n" + 
				"    .$$$?F     ;                       $$$$\"\n" + 
				"    $$$$$$eeu. >                       >P\"\n" + 
				"     `\"\"???$$$$$eu,._''uWb,            )\n" + 
				"               `\"\"??$P$$$$$$b.         :\n" + 
				"                >     ?$$$\"'           {\n" + 
				"                F      `\"              `:\n" + 
				"                >                       `>\n" + 
				"                >                        ?\n" + 
				"               J                          :\n" + 
				"               X                ..  .     ?\n" + 
				"               \"{ 4{!~;/'!>{`~{>~.>! ~! '\"\n" + 
				"                '>!>=.%=.;~~>~4~`{'>>>~!\n" + 
				"                 4'!/>!\\\\!{~~:/{;!{;`;/=':\n" + 
				"                 `=;!~:`~!>{.-; \"(>=.':!;'\n" + 
				"                  :;=.~{`;`~>!~> ?!/>>~!!{'\n" + 
				"                  ~:~'!!;`;`~:>); ;(.uJL!~~\n" + 
				"                    >L.(.:,L;L:-+d$$$$$$\n" + 
				"                    :4$$$$$$$L   ?$$#$$$>\n" + 
				"                     '$$$B$$$>    $$$MB$&\n" + 
				"                      $$$$$$$      $$$@$F\n" + 
				"                      `$$$$$$>     R$$$$\n" + 
				"                       $$$$$$     {$$@$P\n" + 
				"                       $R$$$R     `!)=!>\n" + 
				"                       $$$6T       $$$$'\n" + 
				"                       $$R$B      ;$$$F,._\n" + 
				"                       !=!(!    .'        ``= .\n" + 
				"                       $$$$F    (.             '\\\n" + 
				"                     ,{$$$$(      ``~'`` --:.._.,)\n" + 
				"                    ;   ``  `-.\n" + 
				"                    (          \"\\.\n" + 
				"                     ` -{._       \".\n" + 
				"                           `~:,._ .:\n" + 
				"\n");
	}
	
	public static void affichageHero() {
		System.out.print("\n                  ***********\n" + 
				"               ***** ***********\n" + 
				"            ** ****** *** ********\n" + 
				"           ****  ******  ** *******\n" + 
				"           ***     ******* ** ******\n" + 
				"           ***       **        *  **\n" + 
				"            *|/------  -------\\ ** *\n" + 
				"             |       |=|       :===**\n" + 
				"              |  O  |   | O   |  }|*\n" + 
				"               |---- |   ----  |  |*\n" + 
				"               |    |___       |\\/\n" + 
				"               |              |\n" + 
				"               \\   -----     |\n" + 
				"                \\           |\n" + 
				"                  -__ -- -/\n" + 
				"\n");
	}
	
	public static void affichageCharlotte() {
		System.out.print("\n\nMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMf\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM .xHMMMMMnx\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM(``\"\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\"```\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXMMMMMMMMMMMMMMMMM!XMMMMMMMMMMnHx.\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXMMMMMMMMMMMMMMMMXMMMMMMMMMMMM#``^\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMLMMMMMMMMMMMMMM!MM!MMMMMMMMk..\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM?MMMMMMMMMMMMXMXMMMMMMMMMM:`%?\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMX?MMMMXMMMMMMMMMMX!MMMMMMMMMMMMMh.\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHX?*MMHxMMMMMMMX4MMMMMMMMMMMMMMMM\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMn?MMMMMMMMHMMMM?XHMMMMMMMMMM\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXMMMMMMMMMMXMMMMMMMMMMMMMMX\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMML\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\"``MMMMMMMMMM'M>MMMMP*MMMMMMMMM\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM#     MMMMMMMMM~XM ?M?MMMMn(*MMMMM:\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM*~      'MMMMMMMM! M! 'M ?MMMMMMMX#MMM:\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\"         MMMMMMMf!~XM  'M  MMMMMMMMMMXMM\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\"         :MMMMMMM~!  Mf  'X  XMMnX?MMMMMMMh::-\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\"          dMMMMM\"` !  XM   Xf  XXMMMMMX#MMMMM~^\"\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMMf          .MMMMM~   '  'M~   M~  XfXMMMMMMXMMMMX\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMM        .dMMMMM!     '  XX   'M   X>'X?MX?MMMMMMM>\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMMMMMM     .XMMMM?M!~         X>   M    X  MM?MMXMMMMMMX%\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMMM!`     !~~`    !~           X>  X~   :f  XMMXMMMX#MMMM>\n" + 
				"MMMMMMMMMMMMMMMM*?)XMMM~     %!      :!             '> :\"    X   'MMMMMMM   `*X\n" + 
				"MMMMMMMMMMMMMMMMMMMMMMX     '~      %)XMMMMhx..     'X:\"    .xHMMMMMMMMM~\n" + 
				"MMMMMMMMMMMMMMMMMMMM#X>         -\"\"~       ^\"*MMM*`  X~   :MM*\"   MMMMMX\n" + 
				"MMMMMMMHHnnnXXXnHMMMMM>           ~ .:::xxxxx.      :`>   X(x::: MMMMMMX\n" + 
				"MMMMMMMMMMMMMMMMMMMMMM>          f XMMMMMMM!x.':      `   HMMMhxMMMMMMMf\n" + 
				"MMMMMMMMMMMMMMMM*?dMMM          !  'M~\"*!!   ~?:       : '!?!~ !MMMMMMM>\n" + 
				"MMMMMMHC???)XnHMMMMMMM         !     ~\"~~~\"~ ``':         \"~~`  4MMMMXML\n" + 
				"MMMMMMMMMMMMMMMMMM\"XMf        :                 '                MM!XMMX\n" + 
				"MMMMM\"\\...dM*\"MMMMMMM         >                                  `hMMMM>\n" + 
				"MMMMX`>       `MMMMM\"        '                                    MMMMM>\n" + 
				"MMMMX !         MMM~         !                                    XMMMM\n" + 
				"\")HMX '     '   % ! `.                                            XMMM~\n" + 
				"MMMMM      ~!X ` '   '.                                           XMM~\n" + 
				"MMMMMM       !   !                                                MM\n" + 
				"MMMMMMX         :       `                                         M\n" + 
				"MMMMMMMM                                               .     :   ' \"x\n" + 
				"MMMMMMMMML xx !!                                      `     !    !\n" + 
				"MMMMMMMMMM(~!!~~                                        ::!~    '\n" + 
				"MMMMMMMMMMM.`!  L                                               !\n" + 
				"MMMMMMMMMMMMX!! !                                              %\n" + 
				"MMMMMMMMMMMMM!  '                                  .xnMM:::    >\n" + 
				"MMMMMMMMMMMM!MX  !                              xMMMMMMMMMMM  !\n" + 
				"MMMMMMMMMMMXMMMX '                           ~MMM?!!!!!!!!!?M%\n" + 
				"MMMMMMMMMMMM!MMMX !                             \"X!!`~!!!!!!~f\n" + 
				"MMMMMMMMMMMMM!MMMx !                               \"%!XXX!\" :\n" + 
				"MM#)HMMMMMMMMM!MMM> `.                                      f\n" + 
				"MMMMMMMMMMMMM!MMMMX   '\\                                   !\n" + 
				"MMMMMMMMMMMMXXMMMM'>     `:                                >\n" + 
				"\"?XHMMMMMMMMM!MMMX !       `!:.                           :\n" + 
				"MMMMMMMMMMMMMX!MM  !         `!!!x:.                      f\n" + 
				"MMMMMMMMMMMMMM!M                ~!!!!!!!?!+:....      ..+`\n" + 
				"MMMMMMMMMMMMMM!                    `~!!!!!!!!MMMM\n" + 
				"MMMMMMMMMMMMM!                       `!!!!!!XMMMMk\n" + 
				"MMMMMMMMMMMMf                          `!!!!XMMMMM>\n" + 
				"");
		
		System.out.print("\n\n   _____ _                _       _   _                   \n" + 
				"  / ____| |              | |     | | | |                  \n" + 
				" | |    | |__   __ _ _ __| | ___ | |_| |_ ___             \n" + 
				" | |    | '_ \\ / _` | '__| |/ _ \\| __| __/ _ \\            \n" + 
				" | |____| | | | (_| | |  | | (_) | |_| ||  __/  _   _   _ \n" + 
				"  \\_____|_| |_|\\__,_|_|  |_|\\___/ \\__|\\__\\___| (_) (_) (_)\n" + 
				"                                                          \n" + 
				"                                                          \n\n");
	}
	
	/*
	 * Petites fonctions d'init lors du chargement d'une partie.
	 */
	
	static void initJeu(Initialisation jeuInit) {
		Personnage hero = jeuInit.initPerso();
		Inventaire sac = jeuInit.initInv(hero);
		hero.inventaire = sac;
		classeMain.setHero(hero);		
	}
	
	static void initScript(Initialisation jeuInit) {
		classeMain.jeu = jeuInit.initScene(classeMain.getHero());
	}
	
	static void initIndiceScene(int nb) {
		classeMain.indiceScene = nb;
	}

	/*
	 * Méthodes pour recup ou modifier le hero en cours
	 */
	
	public static Personnage getHero() {
		return hero;
	}

	public static void setHero(Personnage hero) {
		classeMain.hero = hero;
	}
}