import java.util.Scanner;

public class PlusOuMoins extends Epreuve{
	int chiffreJeu;
	
	
	
/***************************************************************************************************
								  __  __      _   _           
								 |  \/  |    | | (_)          
								 | \  / | ___| |_ _  ___ _ __ 
								 | |\/| |/ _ \ __| |/ _ \ '__|
								 | |  | |  __/ |_| |  __/ |   
								 |_|  |_|\___|\__|_|\___|_|   

***************************************************************************************************/
	
	
	/*
	 * Constructeur
	 */
	PlusOuMoins(int id){
		this.ID = id;
		this.chiffreJeu = this.initPlusOuMoins();
		this.tentative = 10;
	}
	
	/*
	 * nombre random à deviner entre 0 et 99
	 */
	int initPlusOuMoins() {
		return (int) ((Math.random()*1000) % 100);
	}
	
	/*
	 * Compare entre la proposition de l'utilisateur et le nombre du "plus ou moins"
	 * Si egale, renvoie true
	 */
	boolean comp(int nb) {
		boolean valid = false;
		String info;
		
		if(nb < this.chiffreJeu) {
				info = "Plus";
				this.tentative -= 1;
		}
		else if(nb > this.chiffreJeu) {
				info = "Moins";
				this.tentative -= 1;
		}
		else {
			info = "Egale";
			valid = true;
		}
		
		System.out.print("Réponse : " + info);
		return valid;
	}
	
	/*
	 * Méthode permettant d'executer la classe PlusOuMoins
	 */
	int play(Personnage hero) throws InterruptedException {
		int idSuivant = this.idSceneSuivante1;
		
		boolean finPartie = false;
		int nb;
		System.out.print("\n\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n");
		System.out.print("\nJeu du plus ou moins\n\n");
		this.afficherDescription(); // Affiche le descriptif de la scene
		
		while(!finPartie) {
			System.out.print("\n\n-------------------------------\n");
			System.out.print("Tentative : " + this.tentative + "\n"); 
			nb = this.choixNombre(); // recup la proposition du joueur
			
			if(finPartie = this.resultatEssaie(nb)) { //verif si le nombre à était trouvé
				System.out.print("\n\nPARTIE GAGNE\n");
				
				if(this.ID == 23) { // Si Plus ou moins de la scene d'on l'id et egale à 23
					/*
					 * Donne l'objet si dessous si victoire
					 */
					equi = new Equipement(10, "Armure du flirt", 20, 0);
					classeMain.afficheArmure();
					System.out.print("Vous avez gagné : " + equi.bonusObjet + ".\nDonne : " + equi.valeurArmure + " de résistance aux contres d'un flirt");
					Inventaire.addObjetInventaire(equi);
					Thread.sleep(2000);
				}
				if(this.ID == 30) {// Si Plus ou moins de la scene d'on l'id et egale à 30
					if(Inventaire.rechercheId(11, classeMain.getHero())) {
						/*
						 * Objet deja present
						 */
						System.out.print("\nObjet deja gagner .. \n");
					}else {
						/*
						 * Donne l'objet si dessous si victoire, et si pas deja gagné
						 * Cette objet est suceptible d'etre gagné plusieur fois, donc il faut controlé si non present
						 */
						equi = new Equipement(11, "Entrainement fifa", 20, 0);
						classeMain.afficheFifa();
						System.out.print("Vous avez gagné : " + equi.bonusObjet + ".\nDonne : " + equi.valeurArmure + " de résistance au match fifa");
						Inventaire.addObjetInventaire(equi);
						Thread.sleep(2000);
					}
				}
				/*
				 * Reinitialise le "plus ou moins"
				 */
				this.tentative = 10;
				this.chiffreJeu = this.initPlusOuMoins();
			}
			
			/*
			 * Perdu, l'objet ne sera pas gagné
			 */
			if(this.tentative == 0) { 
				finPartie = true;
				System.out.print("\n\nVous avez perdu l'épreuve .. \n\n");
			}
		}
		
		System.out.print("\n\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n");
		
		return idSuivant;
	}
	
	/*
	 * recup l'essaie
	 */
	int essaie() {
		int rep = recupVal();
		return rep;
	}
	
	/*
	 * compare si nb trouvé
	 */
	boolean resultatEssaie(int nb){
		return comp(nb);
	}
	
/***************************************************************************************************
							  _____  _       _                        
							 |  __ \(_)     | |                       
							 | |  | |_  __ _| | ___   __ _ _   _  ___ 
							 | |  | | |/ _` | |/ _ \ / _` | | | |/ _ \
							 | |__| | | (_| | | (_) | (_| | |_| |  __/
							 |_____/|_|\__,_|_|\___/ \__, |\__,_|\___|
							                          __/ |           
							                         |___/            

***************************************************************************************************/

	
	
	void afficherDescription(){
		System.out.print(this.descrition);
	}
	
	
	int choixNombre(){
		System.out.print("Votre choix de nombre : ");
		int nb = essaie();
		return nb;
	}

	
	
/***************************************************************************************************

							   _____            _        //\  _      
							  / ____|          | |      |/ \|| |     
							 | |     ___  _ __ | |_ _ __ ___ | | ___ 
							 | |    / _ \| '_ \| __| '__/ _ \| |/ _ \
							 | |___| (_) | | | | |_| | | (_) | |  __/
							  \_____\___/|_| |_|\__|_|  \___/|_|\___|
						                                          

***************************************************************************************************/

	
	int recupVal() {
		int rep = -1;
		
		Scanner sc = new Scanner( System.in ) ; 
		rep = sc.nextInt();
		
		return rep;
	}
	
}
