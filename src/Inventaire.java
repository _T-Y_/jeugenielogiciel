import java.io.Serializable;
import java.util.*;

public class Inventaire implements Serializable{

	private static final long serialVersionUID = 1L;	
	List<Outil> sac;
	public static Personnage hero;
	
	/***************************************************************************************************
									  __  __      _   _           
									 |  \/  |    | | (_)          
									 | \  / | ___| |_ _  ___ _ __ 
									 | |\/| |/ _ \ __| |/ _ \ '__|
									 | |  | |  __/ |_| |  __/ |   
									 |_|  |_|\___|\__|_|\___|_|   

	 ***************************************************************************************************/
	
	Inventaire(Personnage perso){
		this.sac = new ArrayList<Outil>();
		Inventaire.hero = perso;
	}

	
	/*
	 * Permet de voir si un item est présent dans l'inventaire grace à son id
	 */
	public static boolean rechercheId(int nbId, Personnage hero) {
		boolean present = false;
		
		int nbElement;
		nbElement = hero.inventaire.sac.size();
		//System.out.print("Nombre d'element dans le sac : " + nbElement);
		 	
		//System.out.print("\n\n************************************\nRecherche de l'outil avec l'id = " + nbId);
		 
		 for(int i = 0; i < nbElement; i++) {
			 if(hero.inventaire.sac.get(i).id == nbId) {
				 // Présent dans l'inventaire
				 //System.out.print("\n\nPrésent dans l'inventaire\n\n");
				 present = true;
			 }
		 }
		
		return present;
	}
	
	/*
	 * Permet de faire des actions avec les items en stock grace à son id
	 */
	public static int actionObjetOutil(int nbId) {
		int nbElement;
		Personnage hero = classeMain.getHero();
		nbElement = hero.inventaire.sac.size();
		//System.out.print("Nombre d'element dans le sac : " + nbElement);
		 	
		//System.out.print("\n\n************************************\nRecherche de l'outil avec l'id = " + nbId);
		int i; 
		
		Outil obj = null;
		
		 for(i = 0; i < nbElement; i++) {
			 if(hero.inventaire.sac.get(i).id == nbId) {
				 // Présent dans l'inventaire
				 //System.out.print("\n\nPrésent dans l'inventaire\n\n");
				 obj = hero.inventaire.sac.get(i);
				 
				 if(obj.getClass().equals(Clef.class)) {
					Clef recupClef = (Clef) obj;
					System.out.print("\nObjet clef : " + recupClef.bonusObjet);
				}
				if(obj.getClass().equals(Equipement.class)) {
					Equipement recupEquipement = (Equipement) obj;
					System.out.print("\nObjet Equipement : " + recupEquipement.bonusObjet);
				}
				break;
			 }
		 }
		 
		 return i;
	}
	
	/*
	 * Supprime un objet de l'inv
	 */
	public static void supprimerObjetInventaire(int indice) {
		Inventaire.hero = classeMain.getHero();
		hero.inventaire.sac.remove(indice);
		classeMain.setHero(Inventaire.hero);
	}
	
	/*
	 * Ajoute un objet à l'inv
	 */
	public static void addObjetInventaire(Outil obj) {
		//Inventaire.hero = classeMain.getHero();
		Personnage h = classeMain.getHero();
		hero.inventaire.sac.add(obj);
		classeMain.setHero(h);
	}
}