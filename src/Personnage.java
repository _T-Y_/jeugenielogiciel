import java.io.Serializable;
import java.util.*;


public class Personnage implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String prenom; 
	short vie;
	short vie_max; 
	Inventaire inventaire;

	
	/***************************************************************************************************
										  __  __      _   _           
										 |  \/  |    | | (_)          
										 | \  / | ___| |_ _  ___ _ __ 
										 | |\/| |/ _ \ __| |/ _ \ '__|
										 | |  | |  __/ |_| |  __/ |   
										 |_|  |_|\___|\__|_|\___|_|   
                              
	***************************************************************************************************/

	
	Personnage(){
		/** Constructeur de la classe Personnage */
		this.affichageChoixHero();
		this.prenom = this.choixHero();
		this.vie = 100;
		this.vie_max = 100;
	}

	/*
	 * apres un affrontement, la vie et reinitialisé
	 */
	public static void reinisialiseVie()
	{
		/**Reinisialise la vie du joueur apres les affrontements */
		Personnage hero = classeMain.getHero();
		hero.vie = hero.vie_max;
		classeMain.setHero(hero);
	}
	
	
	
	/***************************************************************************************************
								  _____  _       _                        
								 |  __ \(_)     | |                       
								 | |  | |_  __ _| | ___   __ _ _   _  ___ 
								 | |  | | |/ _` | |/ _ \ / _` | | | |/ _ \
								 | |__| | | (_| | | (_) | (_| | |_| |  __/
								 |_____/|_|\__,_|_|\___/ \__, |\__,_|\___|
								                          __/ |           
								                         |___/            

	 ***************************************************************************************************/
	
	public void affichageChoixHero() {
		System.out.print("\n\nVeuillez nommer le héro de votre Histoire : ");
	}
	
	
	/***************************************************************************************************

									   _____            _        //\  _      
									  / ____|          | |      |/ \|| |     
									 | |     ___  _ __ | |_ _ __ ___ | | ___ 
									 | |    / _ \| '_ \| __| '__/ _ \| |/ _ \
									 | |___| (_) | | | | |_| | | (_) | |  __/
									  \_____\___/|_| |_|\__|_|  \___/|_|\___|
                                                                             

	 ***************************************************************************************************/

	public String choixHero() {
		String prenom;
		Scanner sc = new Scanner(System.in);
		prenom = sc.nextLine();
		return prenom;
	}
	
}
