import java.io.IOException;
import java.util.Scanner;

//package jeugenielogiciel;

public abstract class Scene {
	
	int ID;	
	int idSceneSuivante1;
	int idSceneSuivante2;
	int idSceneSuivante3;
	
	String SceneSuivante1;
	String SceneSuivante2;
	String SceneSuivante3;
	
	public static int IdMax = 50;
	
	String descrition;
	/*
	private Outil obj;
	
	public Outil getObj() {
		return obj;
	}
	
	public void setObj(Outil obj) {
		this.obj = obj;
	}
	*/
	
	/***************************************************************************************************
									  __  __      _   _           
									 |  \/  |    | | (_)          
									 | \  / | ___| |_ _  ___ _ __ 
									 | |\/| |/ _ \ __| |/ _ \ '__|
									 | |  | |  __/ |_| |  __/ |   
									 |_|  |_|\___|\__|_|\___|_|   
	 * @throws InterruptedException 

	 ***************************************************************************************************/
	
	/*
	 * Permet de faire une action sur la scene envoyé en parametre.
	 * Permet d'executer la scene
	 * 
	 * Des castes permettent de determiner le type des scene.
	 * Si c'est un parcoursSimple, un Affrontement ou une epreuve 
	 */
	public static int actionScene(Scene obj, Personnage hero, Memoire mem) throws IOException, InterruptedException {
		int idSuivant = 0;
		PlusOuMoins recupPOM = null;
		Pendu recupP = null;
		Affrontement recupE = null;
		ParcoursSimple parcours = null;
		
		if(obj.getClass().equals(PlusOuMoins.class)) {
			//System.out.print("\nType Plus ou Moins\n");
			recupPOM = (PlusOuMoins) obj;
			idSuivant = recupPOM.play(hero);
		}
		else if(obj.getClass().equals(Pendu.class)) {
			//System.out.print("\nType Pendu\n");
			recupP = (Pendu) obj;
			idSuivant = recupP.play(hero,mem);
		}
		else if(obj.getClass().equals(Affrontement.class)) {
			//System.out.print("\nType Epreuve\n");
			recupE = (Affrontement) obj;
			idSuivant = recupE.play(hero);
		}
		else if(obj.getClass().equals(ParcoursSimple.class)) {
			parcours = (ParcoursSimple) obj;
			idSuivant = parcours.play(hero,mem);
		}
		else {
			System.out.print("\nERROR : Ne reconnait pas le type\n");
		}
		
		return idSuivant;
	}
	
}
