import java.io.Serializable;

public class Clef extends Outil implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Clef(int i, String descriptionClef){
		/** Cree un outil de type Equipement */
		this.id = (short) i;
		this.bonusObjet = descriptionClef;
	}
}