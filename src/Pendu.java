import java.io.IOException;
import java.util.*;

//package jeugenielogiciel;


public class Pendu extends Epreuve{
	String[] listePendu;
	String motJeu;
	String lettresDuJoueur;
	
	
	/***************************************************************************************************
										  __  __      _   _           
										 |  \/  |    | | (_)          
										 | \  / | ___| |_ _  ___ _ __ 
										 | |\/| |/ _ \ __| |/ _ \ '__|
										 | |  | |  __/ |_| |  __/ |   
										 |_|  |_|\___|\__|_|\___|_|   
                              
	***************************************************************************************************/
	
	
	/*
	 * Constructeur : charge un mot de la liste présente dans -> motJeu
	 */
	
	/*
	 * init du pendu
	 * La liste est une liste de mot auquel le pendu va en choisir un aléatoirement
	 */
	Pendu(int id, String[] liste) {
		
		// Initialisation
		this.listePendu = liste;
		this.motJeu = listePendu[	(int) (((Math.random())*100)	%	(liste.length))	];
		this.tentative = 10;
		this.ID = id;
		this.lettresDuJoueur = new String();
	}
		

	/*
	 * si lettre presente dans le mot choisi
	 */
	boolean verifLettre(char lettre) {
		if (motJeu.indexOf( lettre ) != -1) {
			return true;
		}
		return false;
	}
	
	
	/*
	 * concatenation avec les lettres deja trouvé
	 * permet d'avoir un visuel sur les lettres deja faite
	 */
	void enregistreLettre(char lettre){
		this.lettresDuJoueur += lettre;
	}
	
	
	
	int controleLongeurMot(String mot) {
		/*
		 * un nombre négatif si la chaîne actuelle est placée avant la chaîne passée en paramètre;
		 * zéro (0) si les deux chaînes sont strictement égales;
	     * un nombre positif si la chaîne actuelle est placée après la chaîne passée en paramètre.
		 */
		return mot.compareTo(this.motJeu);
	}
	

	/*
	 * Enleve 3 points pour une tentative d'un mot qui s'avere faut
	 */
	void tentativeMotFault() {
		this.tentative -= 3;
	}
	

	/*
	 * Si mot proposé est egale au mot du pendu
	 */
	boolean conroleMotTrouve(String mot){
		return mot.equals(this.motJeu);
	}

	/*
	 * Controle si avec la lettre proposé,si le mot et trouvé
	 */
	boolean motValidTentativeLettre() {
		boolean motValide = false;
		
		for(int i = 0; i < this.motJeu.length(); i++) {
			motValide = false;
			for(int j = 0; j < this.lettresDuJoueur.length(); j++) {
				if(this.motJeu.charAt(i) == this.lettresDuJoueur.charAt(j)) {
					motValide = true;
					break;
				}
			}
			if(motValide == false)
					break;
		}
		return motValide;
	}
	
	/*
	 * Méthode d'execution de la classe Pendu
	 */
	int play(Personnage hero, Memoire mem) throws IOException, InterruptedException {
		int idSuivant = this.idSceneSuivante1;

		boolean finDuPendu = false;
		boolean gagne = true;
		
		/*
		 * Init l'objet à gagné
		 */
		equi = new Equipement(20, "Réthorique de l'amour" , 0, 20);
		
		System.out.print("\n\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n");
		
		this.afficherDescription(); // affiche la description de la scene de ce pendu
		
		while(!finDuPendu) {
			System.out.print("\n\n-----------------------\n\n");
			
			System.out.print("Le mot à trouver : ");
			this.afficherLettreTrouve(); // affiche les lettres trouvees du mot
			System.out.print("\n\n");
			
			System.out.print("Vos essaies : ");
			this.afficheLettreTest();// affiche les lettres testé
			System.out.print("\n\n");
			
			System.out.print("Nombre disponible de tentative : "+this.tentative+"\n");
			
			/*
			 * Si on veut proposer une lettre ou un mot directement
			 */
			if(this.afficheChoix() == 0) { // 0 mot, 1 lettre
				finDuPendu = this.tentativeMot(); // si mot trouver
			}else {
				finDuPendu = this.tentativeLettre();// si mot trouver
			}
			
			if(this.tentative == 0) { // fin de l'épreuve si toute les tentatives sont consommées
				finDuPendu = true;
				gagne = false;
			}
		}
		
		if(gagne) {
			// Donne l'objet
			classeMain.afficheRetoriqueDeLAmour();
			System.out.print("Vous avez gagné : " + equi.bonusObjet + ".\nDonne : " + equi.valeurAttaque + " % de chance de succés au flirt");
			Inventaire.addObjetInventaire(equi);
			Thread.sleep(2000);
		}else {
			// Ne donne pas l'objet
			System.out.print("Vous avez perdu\n");
		}
		System.out.print("\n\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n");
		return idSuivant;
	}
	
	
	/*
	 * Recup le mot pour le tester avec celui du pendu
	 */
	boolean tentativeMot(){
		System.out.print("Votre mot : ");
		String mot = recupMot();
		
		int essaie = this.controleLongeurMot(mot);
		//System.out.print("\nOK\n");
		if(essaie == 0) {
			if(this.conroleMotTrouve(mot)) {
				return true;
			}
		}
		this.tentativeMotFault();
		return false;
	}
	

	/*
	 * Recup la lettre et vois si elle compose la mot du pendu
	 */
	boolean tentativeLettre() {
		System.out.print("Votre lettre : ");
		boolean motTrouve = false;
		
		char lettre = this.recupLettre();
		enregistreLettre(lettre);
		
		if(this.verifLettre(lettre)) {
			motTrouve = this.motValidTentativeLettre();
		}else {
			this.tentative -= 1;
		}
		
		return motTrouve;
	}
	
	
	/***************************************************************************************************
								  _____  _       _                        
								 |  __ \(_)     | |                       
								 | |  | |_  __ _| | ___   __ _ _   _  ___ 
								 | |  | | |/ _` | |/ _ \ / _` | | | |/ _ \
								 | |__| | | (_| | | (_) | (_| | |_| |  __/
								 |_____/|_|\__,_|_|\___/ \__, |\__,_|\___|
								                          __/ |           
								                         |___/            

	 ***************************************************************************************************/
	
	
	void afficherDescription(){
		System.out.print(this.descrition);
	}
	

	void afficheLettreTest() {
		for(int i = 0; i < this.lettresDuJoueur.length(); i++) {
			System.out.print(this.lettresDuJoueur.charAt(i));
		}
	}
	
	void afficherLettreTrouve() {
		for(int i = 0; i < this.motJeu.length(); i++) {
			this.AfficheLettre(this.motJeu.charAt(i));
		}
		System.out.print("\n");
	}
	
	/*
	 * affiche la lettre du mot a deviné si on la deja proposé
	 */
	void AfficheLettre(char lettre) {
		boolean affiche = false;
		
		for(int j = 0; j < this.lettresDuJoueur.length(); j++) {			
			if(this.lettresDuJoueur.charAt(j) == lettre)
				affiche = true;
		}
		
		if(affiche)
			System.out.print(lettre + " ");
		else
			System.out.print("_ ");
			
	}
	
	int afficheChoix() {
		System.out.print("\n\n\tATTENTION : saisir un chiffre ici ..\nVoulez-vous tenter le mot(0) ou une lettre?(1)\nVotre choix : ");
		int rep;
		
		Scanner sc = new Scanner(System.in);
		rep = sc.nextInt();
		
		if(rep < 2 && rep > -1)
			return rep;
		
		return afficheChoix();
	}
	
	
	
	/***************************************************************************************************

									   _____            _        //\  _      
									  / ____|          | |      |/ \|| |     
									 | |     ___  _ __ | |_ _ __ ___ | | ___ 
									 | |    / _ \| '_ \| __| '__/ _ \| |/ _ \
									 | |___| (_) | | | | |_| | | (_) | |  __/
									  \_____\___/|_| |_|\__|_|  \___/|_|\___|
                                                                             

	 ***************************************************************************************************/
	
	
	String recupMot() {
		String mot;
		
		Scanner sc = new Scanner(System.in);
		mot= sc.next();
		
		return mot;
	}
	
	char recupLettre() {
		char lettre;
		
		Scanner sc = new Scanner(System.in);
		lettre= sc.next().charAt(0);
		
		return lettre;
	}

}