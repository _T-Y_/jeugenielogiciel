import java.io.IOException;
import java.util.Scanner;

public class Menu {

	/***************************************************************************************************
									  _____  _       _                        
									 |  __ \(_)     | |                       
									 | |  | |_  __ _| | ___   __ _ _   _  ___ 
									 | |  | | |/ _` | |/ _ \ / _` | | | |/ _ \
									 | |__| | | (_| | | (_) | (_| | |_| |  __/
									 |_____/|_|\__,_|_|\___/ \__, |\__,_|\___|
									                          __/ |           
									                         |___/            

	 ***************************************************************************************************/

	/*
	 * Permet d'orienter si le joueur veut sauvegarder, charger ou quitter.
	 * 
	 * ATTENTION : Ne pas charger si aucune sauvegarde n'a était faite. 
	 */
	
	static int afficheQuitter() {
		System.out.print("Voulez vous quitter? oui(0) / non(1)\n");
		return quitter();
	}
	

	public static int afficheMenu(int scene, Personnage hero, Memoire mem) throws IOException{
		int choix, rep; 
		System.out.print("\nAcceder à la mémoire? oui(0) / non(1)");
		choix = choixMenu();
		
		if(choix == 0) {
			if( mem.play(scene, hero, hero.inventaire) == 1 )
				return 2;
			else
				return 0;
		}else {
			rep = afficheQuitter();
			if(rep == 0)
				return 1;
		}
		
		System.out.print("\n\n");
		return 0;
	}
	
	/***************************************************************************************************
							
								   _____            _        //\  _      
								  / ____|          | |      |/ \|| |     
								 | |     ___  _ __ | |_ _ __ ___ | | ___ 
								 | |    / _ \| '_ \| __| '__/ _ \| |/ _ \
								 | |___| (_) | | | | |_| | | (_) | |  __/
								  \_____\___/|_| |_|\__|_|  \___/|_|\___|
                                       

	 ***************************************************************************************************/

	
	static int quitter() {
		int choix = 0;
		
		System.out.print("\nVotre choix : ");
		
		Scanner sc = new Scanner(System.in);
		choix = sc.nextInt();
		
		if(choix <= 3 && choix >= 0)
			return choix;
		
		System.out.print("Mauvaise saisie\n");
		return quitter();
	}
	
	public static int choixMenu() {
		int choix = 0;
		
		System.out.print("\nVotre choix : ");
		
		Scanner sc = new Scanner(System.in);
		choix = sc.nextInt();
		
		
		if(choix == 0 || choix == 1)
			return choix;
		
		System.out.print("\nChoix non valide\n");
		return choixMenu();
	}
	
}
