//package jeugenielogiciel;

public class Initialisation {
	Initialisation(){
		//System.out.print("\n****************************\nInitialisation du jeu\n****************************\n");
	}
	
	public Personnage initPerso() {
		Personnage hero = new Personnage();
		return hero;
	}
	
	public Inventaire initInv(Personnage hero) {
		Inventaire sac = new Inventaire(hero);
		return sac;
	}
	
	
	/*
	 * Ci dessous le tableau de scene est initialisé.
	 * Chaque script est initialisé dans sa description
	 */
	public Scene[] initScene(Personnage hero) {
		int id = 0;
		
		Scene jeu[] = null;
		jeu = new Scene[Scene.IdMax];
		
		/* 
		 * INTRODUCTION
		 */
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "\nA la fin des fêtes de Noël, " + hero.prenom + " avait repris les cours au sein de son Lycée. Pour cette nouvelle année il avait pris une nouvelle résolution : \nAvoir confiance en soi ! Il avait besoin de cette résolution pour trouver enfin une petite amie. Enfin ce n'était pas vraiment la seule dont il avait besoin… \nIl devait aussi apprendre à s'exprimer, ne plus avoir peur de parler aux filles, savoir choisir le bon moment, arrêter de paniquer au moment \nles plus important et bien plus… Bref, le plus important c'était la confiance en soi car auparavant sa confiance, presque inexistante, \nlui avait coûté au moins 50 râteaux au collège... C'était l’année où il allait changer la courbe de tendance ! \n" + 
				"\n" + 
				"Le mercredi de la rentrée, il croise cette jeune fille en seconde, comme lui, blonde aux yeux clairs. Lors de la pause de midi, \nle regard de cette fille avait fait fondre " + hero.prenom + ". Frappé par la foudre de l’amour, il voulait la connaître un peu plus. \nC'était le moment de mettre en œuvre ses nouvelles résolutions ! Mais comment allait-il faire le premier pas vers cette merveilleuse fille ? \nEntouré de sa meilleure amie et des quelques amis qu’allais faire " + hero.prenom + "...\n" + 
				"";
		jeu[id].idSceneSuivante1 = 1;
		jeu[id].SceneSuivante1 = "Attendre pour une meilleur situation";
		jeu[id].idSceneSuivante2 = 2;
		jeu[id].SceneSuivante2 = "Aller lui parler maintenant !";
		jeu[id].idSceneSuivante3 = 3;
		jeu[id].SceneSuivante3 = "Envoyer un message plus tard";
		
		/*
		 * Attendre pour une meilleur situation
		 */
		
		id = 1;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = hero.prenom + " décide donc d’attendre un peu plus pour aller parler à cette fille, le temps qu’elle ne soit pas entourée d’autant de personnes. \nC’est sûrement une bonne stratégie pour ne pas devenir un intrus parmi les amis de cette ravissante fille. \nCependant elle semble bien occupée et pas prête du tout à se séparer de ses amis, quand est ce que " + hero.prenom + " pourra t-il la voir… \n" + hero.prenom + " étant, depuis son plus jeune âge, très patient, il attendis la meilleure opportunité. \nMalheureusement celle-ci a eu lieu trois jours après… Oui chère lectrice et cher lecteur, " + hero.prenom + " avait bien cours le samedi matin. \nCe jour-là, elle était toute seule sans ses amis pendant un petit instant… \n" + hero.prenom + " avait trouvé le bon moment mais qu’allait-il lui dire ? ";
		jeu[id].idSceneSuivante1 = 4;
		jeu[id].SceneSuivante1 = "Salut, ça va ?";
		jeu[id].idSceneSuivante2 = 5;
		jeu[id].SceneSuivante2 = "Combien pèse un ours polaire";
		jeu[id].idSceneSuivante3 = 6;
		jeu[id].SceneSuivante3 = "Rester Naturel";
		
		/*
		 * Aller lui parler maintenant !
		 */
		
		id = 2;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = hero.prenom + " prend son courage à deux mains, ses nouvelles résolutions en tête et va parler a cette merveilleuse fille. \nMalheureusement il n’avait aucun sujet en tête… Il ne savait même pas le prénom de cette fille. \nIl se retrouva au milieu des regards et ce devint une situation tellement inconfortable pour notre héros… \nCe que nous pouvons apprendre de cette histoire c’est qu' il faut aussi être intelligent et choisir le bon moment… \nC’est ainsi que notre héros avait perdu toute confiance en lui… Game over. \n" +
				"   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		/*
		 * Envoyer un message plus tard
		 */
		
		id = 3;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "C’est ainsi que " + hero.prenom + " décide d’attendre le soir pour lui envoyer un message. Arrivé chez lui, il demande quelques renseignements à ses amis \npour essayer de trouver le prénom de cette fille qui a tant charmé " + hero.prenom + ". \nElle s'appelle Charlotte et est en seconde 3 dans la classe de son ami Pierre. \n" + hero.prenom + " commença à naviguer sur les réseaux sociaux pour trouver Charlotte. Il a vu pendant un moment les merveilleuses photos de Charlottes qui le charmaient encore. \nIl voyait en elle une fille élégante et délicate. Il glissa donc sur les messages privés pour essayer de la contacter. \nMême si Charlotte n'était pas devant lui, il devait quand même envoyer un message qui réussira à attirer l’attention de Charlotte sans se faire bloquer.  \nQue devait t-il lui envoyer ? \n";
		jeu[id].idSceneSuivante1 = 7;
		jeu[id].SceneSuivante1 = "Parler de une de ses photos";
		jeu[id].idSceneSuivante2 = 8;
		jeu[id].SceneSuivante2 = "Parler comme un poete";
		jeu[id].idSceneSuivante3 = 9;
		jeu[id].SceneSuivante3 = "Parler des examens commun des secondes";
		
		/*
		 * Salut, ça va ?
		 */

		id = 4;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = hero.prenom + " décide de faire une approche plutôt connue dans ce milieu. Il s’approche d’elle et\ndit “Salut, ça va? “. Il faut savoir que la première phrase qu’on dit a une fille c’est comme\nenvoyer un curriculum vitae ! Il faut que la phrase soit accrocheuse ! " + hero.prenom + " n’a pas été\ntrès accrocheur mais lui a répondu. C’est ainsi qu' une longue conversation entre Charlotte\net " + hero.prenom + " avait commencé. Ils firent connaissance et cette belle fille s’appelait Charlotte.\n" + 
				"\n" + 
				"Après avoir réussi à faire connaissance, cela ne devait pas s'arrêter là ! Quelques jours \naprès, " + hero.prenom + " croisa Charlotte au lycée. Il voulait lui parler un peu plus en privé.\nCependant son téléphone commença à sonner.  Qu’allais faire " + hero.prenom + " ? \n\n" + 
				"";
		jeu[id].idSceneSuivante1 = 10;
		jeu[id].SceneSuivante1 = "Faut qu’on parle Charlotte";
		jeu[id].idSceneSuivante2 = 11;
		jeu[id].SceneSuivante2 = "Continuer à parler simplement";
		jeu[id].idSceneSuivante3 = 12;
		jeu[id].SceneSuivante3 = "Répondre au téléphone";
		
		/*
		 * Combien pèse un ours polaire
		 */

		id = 5;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = hero.prenom + " avait une idée en tête. Une idée qui lui semblait être l’idée ultime ! Une idée qui \n" + 
				"ferait rire cette fille et ainsi gagner son coeur ! C'était une technique ancestrale non permise\n" + 
				"pendant des siècles… Il arrive devant cette fille avec une confiance en lui monumentale et il \n" + 
				"lui dit “ Tu sais combien pèse un ours polaire ? “. Cette fille était surprise que quelqu’un lui \n" + 
				"pose une question pareille et ne sachant quoi répondre" + hero.prenom + " continue et dit “Assez pour \n" + 
				"briser la glace ! Maintenant que la glace est brisée je m’appelle " + hero.prenom + ". Elle fit un \n" + 
				"léger sourire avant de partir sans se retourner et sans même lui répondre…Nous pouvons \n" + 
				"en conclure que si cette technique était une technique interdite elle doit encore le rester…\n" + 
				"\n   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		/*
		 * Rester Naturel
		 */

		id = 6;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = hero.prenom + " se force à rester naturel. Heureusement pour lui c’est un très bon choix ! Il \n" + 
				"arrive a commencé une discussion avec cette jeune fille qui s’appelle Charlotte ! En parlant \n" + 
				"de tout et de rien pendant quelques instants, qui semblent les meilleurs instant de \n" + 
				""+hero.prenom+" de sa vie, ils firent connaissance et rapidement les deux se sentirent à l'aise \n" + 
				"dans la discussion ! \n" + 
				"\n" + 
				"Une grande connexion s'était établi entre Charlotte et "+hero.prenom+" que les deux sentirent \n" + 
				"qu’ils devaient parler un peu plus dans un endroit un peu plus privé. Charlotte évoque donc \n" + 
				"le fait d’avoir faim et c’est ainsi que "+hero.prenom+" eu une idée géniale ! Il voulait l'inviter à \n" + 
				"manger après les cours. \n" + 
				"";
		jeu[id].idSceneSuivante1 = 13;
		jeu[id].SceneSuivante1 = "Toi, Moi une pizza ?";
		jeu[id].idSceneSuivante2 = 14;
		jeu[id].SceneSuivante2 = "Être direct";
		jeu[id].idSceneSuivante3 = 15;
		jeu[id].SceneSuivante3 = "Tu fais quoi après les cours";
		
		
		/*
		 * Parler de une de ses photos
		 */

		id = 7;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "Sur les réseaux sociaux qu’utilisait Charlotte, elle postait souvent des photos de ses \n" + 
				"voyages d’été. Dans une de ses photos, elle était partie à Venise, un endroit que \n" + 
				"" + hero.prenom + " connaissait. C’est ainsi qu’ils se mirent à parler de leurs aventures à Venise. \n" + 
				"Ils firent connaissance et rapidement ils se sentirent à l’aise dans cette discussion.\n" + 
				"\n" + 
				"Une grande connexion s'était établi entre Charlotte et " + hero.prenom + " que les deux sentirent \n" + 
				"qu’ils devaient parler un peu plus dans un endroit un peu plus privé. Charlotte évoque donc \n" + 
				"le fait d’avoir faim et c’est ainsi que " + hero.prenom + " eu une idée géniale ! Il voulait l'inviter à \n" + 
				"manger !\n" + 
				"";
		jeu[id].idSceneSuivante1 = 13;
		jeu[id].SceneSuivante1 = "Toi, Moi une pizza ?";
		jeu[id].idSceneSuivante2 = 14;
		jeu[id].SceneSuivante2 = "Être direct";
		jeu[id].idSceneSuivante3 = 15;
		jeu[id].SceneSuivante3 = "Tu fais quoi après les cours";
		
		/*
		 * Parler comme un poete
		 */

		id = 8;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = hero.prenom + " eut une drôle d’idée, il s'était rappelé du conseil de son grand-père : “ \n" + 
				"Rappelle toi de ça mon petit fils, les filles adorent les hommes doux qui lui écrivent des \n" + 
				"poèmes…\" C’est comme ça que " + hero.prenom + " se prit pour un poète et lui envoya comme \n" + 
				"premier message :\n" + 
				"\n" + 
				"“Tu sembles un peu excentrique, ce que j’apprécie toujours chez une femme. La normalité \n" + 
				"est trop surestimée de nos jours ! Alors, dis-moi ; si, en chemin pour venir te rencontrer pour \n" + 
				"la première fois, je tombais nez à nez avec ton groupe d’amis et que je leur demandais à \n" + 
				"quoi m’attendre… que me répondraient-ils ?”\n" + 
				"\n" + 
				"Aussitôt envoyé aussitôt " + hero.prenom + " fit bloqué par Charlotte…\n" + 
				"\n" + 
				"Nous pouvons en conclure que les temps changent et on ne doit pas rester ancré dans le \n" + 
				"passé…\n" + 
				"\n   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		/*
		 * Parler des examens commun des secondes 
		 */

		id = 9;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "Ne sachant pas comment commencer une discussion avec Charlotte, il avait choisi d’être \n" + 
				"plutôt discret et commença par lui demander des informations sur les examens communs \n" + 
				"des secondes. Dès la réponse de Charlotte, il essaya d’extraire le plus d'informations d’elle. \n" + 
				"Ils firent simplement connaissance. C’est ainsi que [hero.prenom] devait essayer d’aller un \n" + 
				"peu plus loin. \n" + 
				"\n" + 
				"Après avoir réussi à faire connaissance, cela ne devait pas s'arrêter là ! Quelques jours \n" + 
				"après, " + hero.prenom + " croisa Charlotte au lycée. Il voulait lui parler un peu plus en privé. Mais \n" + 
				"comment va t-il s’y prendre ? \n" + 
				"";
		jeu[id].idSceneSuivante1 = 10;
		jeu[id].SceneSuivante1 = "Faut qu’on parle Charlotte";
		jeu[id].idSceneSuivante2 = 12;
		jeu[id].SceneSuivante2 = "Répondre au téléphone";
		jeu[id].idSceneSuivante3 = 11;
		jeu[id].SceneSuivante3 = "Continuer à parler simplement";
		
		
		
		/*
		 * Faut qu’on parle Charlotte
		 */

		id = 10;
		
		String[] mots = {"truffe","pain-depice","sucrerie"};
		
		jeu[id] = new Pendu(id,mots);
		jeu[id].descrition = hero.prenom + " a bizarrement dit à Charlotte “ Faut qu’on parle”. Une drôle de façon de parler \n" + 
				"à une fille qu’on connait à peine. Charlotte était vraiment surprise et se méfier de " + hero.prenom + ". \n" + 
				"Comme Charlotte était une fille curieuse, elle accepta c’est ainsi qu’il établirent un \n" + 
				"rendez-vous dans cinq minutes près du terrain de football. \n" + 
				"\n" + 
				"Cependant, " + hero.prenom+ " voulait voir le sourire de Charlotte avant le rendez-vous… \n" + 
				"Comme elle partait il lui a dit :  “ A tout à l'heure ma …” \n" + 
				"";
		jeu[id].idSceneSuivante1 = 22;
		//jeu[id].SceneSuivante1 = "";
		//jeu[id].idSceneSuivante2 = ;
		//jeu[id].SceneSuivante2 = "";
		//jeu[id].idSceneSuivante3 = ;
		//jeu[id].SceneSuivante3 = "";
		
		/*
		 * Continuer à parler simplement
		 */

		id = 11;
		
		jeu[id] = new Pendu(id,mots);
		jeu[id].descrition = "[hero.prenom] était confiant et a donc décidé d'aller parler à Charlotte normalement. Ils \n" + 
				"continuèrent leurs discussions sans interruption. La connexion entre ces deux là augmentait. \n" + 
				"C’est ainsi que Charlotte devait faire quelque chose mais comme elle voulait quand même \n" + 
				"continuer la discussion avec [hero.prenom] elle proposa de se voir dans cinq minutes près \n" + 
				"du terrain de football. Évidemment [hero.prenom] avait accepté ! \n" + 
				"\n" + 
				"Cependant, [heros.prenom] voulait voir le sourire de Charlotte avant le rendez-vous… \n" + 
				"Comme elle partait il lui a dit :  “ A tout à l'heure ma …” \n" + 
				"";
		jeu[id].idSceneSuivante1 = 22;
/*		jeu[id].SceneSuivante1 = "";
		jeu[id].idSceneSuivante2 = ;
		jeu[id].SceneSuivante2 = "";
		jeu[id].idSceneSuivante3 = ;
		jeu[id].SceneSuivante3 = "";
*/		
		
		/*
		 * Répondre au téléphone
		 */

		id = 12;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "Lorsque " + hero.prenom + " prit son téléphone, il vit que la personne qui l'appelait était sa mère. \n" + 
				"Pris par des sueurs froides, il se rappela que ce même jour les résultats du premier \n" + 
				"semestre étaient arrivés et que sa mère les avait sûrement vu… Il était obligé de répondre… \n" + 
				"C’est ainsi que Charlotte est partie et notre héros n’as pas eu l'occasion de lui parler… \n" + 
				"\n   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		/*
		 * Toi, Moi une pizza ? 
		 */

		id = 13;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "Avec une surdose de confiance, il lui dit : “ Toi, Moi, une pizza, des vêtements courts ça te \n" + 
				"dis ? “ Ceci choque énormément Charlotte qui est vegan qui ne boit que de l’eau \n" + 
				"déminéralisée et les pizza utilisent du fromage… \n" + 
				"C’est ainsi que se finit l’aventure de notre héros…  Force à lui.\n" + 
				"\n   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		/*
		 * Être direct
		 */

		id = 14;
		
		jeu[id] = new Affrontement(100, "Obelisque le tourmenteur", "Slifer, le dragon céleste", "Le dragon ailé de Ra",id);
		jeu[id].descrition = "Sans hésiter " + hero.prenom + " décide d'être direct ! Il lui dit “ J’aime beaucoup discuter avec toi. \n" + 
				"Aimerais-tu que l’on aille prendre un café ensemble un de ces jours ? “ Eh bien Charlotte \n" + 
				"aime bien les hommes directs qui savent avec douceur montrer leurs intentions. Elle \n" + 
				"accepta avec joie ! Ce qui fit le bonheur de notre héros " + hero.prenom + " ! \n" + 
				"\n" + 
				"Ils décident de sortir prendre un café un soir dans la semaine. C'était la première fois qu' \n" + 
				"une fille accepta une proposition de la part de " + hero.prenom + " ! Le soir en question arriva et \n" + 
				"" + hero.prenom + " se vêtit avec ses vêtements les plus beaux qu’il possédait. Mais en chemin \n" + 
				"vers le restaurant il se rendit compte qu’il ne pouvait arriver les mains vides. \n" + 
				"\n" + 
				"Il décida d’acheter des fleurs pour Charlotte (Chères lectrices et chers lecteurs ne faites pas \n" + 
				"ça pour le premier rendez-vous ). " + hero.prenom + " avait choisi les fleurs mais au moment de \n" + 
				"payer il n’avait point de liquide sur lui. C’est à ce moment-là que le fleuriste lui propose un \n" + 
				"duel avec trois cartes. Il faut choisir aléatoirement une et espérer gagner. \n" + 
				"";
		jeu[id].idSceneSuivante1 = 23;
/*		jeu[id].SceneSuivante1 = "";
		jeu[id].idSceneSuivante2 = ;
		jeu[id].SceneSuivante2 = "";
		jeu[id].idSceneSuivante3 = ;
		jeu[id].SceneSuivante3 = "";
*/		
		
		/*
		 * Tu fais quoi après les cours
		 */

		id = 15;
		
		jeu[id] = new Affrontement(100,"Obelisque le tourmenteur","Slifer, le dragon céleste","Le dragon ailé de Ra",id);
		jeu[id].descrition = hero.prenom + " ne sachant pas comment aborder sa Charlotte, il pensait que lui proposer de \n" + 
				"manière indirecte un café le rendrait plus attractif. \n" + 
				"Seulement, Charlotte connaissait  les intentions de " + hero.prenom + " et trouva alors dommage \n" + 
				"qu’ils ne les fissent pas comprendre plus clairement. Mais il était tellement mignon qu'elle \n" + 
				"voulut lui donner sa chance et accepta d’aller prendre un café. \n" + 
				"\n" + 
				"Ils décident de sortir prendre un café un soir dans la semaine. C'était la première fois qu' \n" + 
				"une fille accepta une proposition de la part de " + hero.prenom + " ! Le soir en question arriva et \n" + 
				"" + hero.prenom + " se vêtit avec ses vêtements les plus beaux qu’il possédait. Mais en chemin \n" + 
				"vers le restaurant il se rendit compte qu’il ne pouvait arriver les mains vides. \n" + 
				"\n" + 
				"Il décida d’acheter des fleurs pour Charlotte (Chères lectrices et chers lecteurs ne faites pas ça pour le premier rendez-vous ). \n" + hero.prenom + " avait choisi les fleurs mais au moment de \n" + 
				"payer il n’avait point de liquide sur lui. C’est à ce moment-là que le fleuriste lui propose un \n" + 
				"duel avec trois cartes. Il faut choisir aléatoirement une et espérer gagner. \n" + 
				"";
		jeu[id].idSceneSuivante1 = 23;
/*		jeu[id].SceneSuivante1 = "";
		jeu[id].idSceneSuivante2 = ;
		jeu[id].SceneSuivante2 = "";
		jeu[id].idSceneSuivante3 = ;
		jeu[id].SceneSuivante3 = "";
*/		
		
		
		/*
		 * Toi, Moi une pizza ?
		 */
		
		id = 16;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "Avec une surdose de confiance, il lui dit : “ Toi, Moi, une pizza, des vêtements courts ça te \n" + 
				"dis ? “ Ceci choque énormément Charlotte qui est vegan qui ne boit que de l’eau \n" + 
				"déminéralisée et les pizza utilisent du fromage… \n" + 
				"C’est ainsi que se finit l’aventure de notre héros…  Force à lui.\n" + 
				"\n   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		/*
		 * AFF: Clash of clans
		 */
		
		id = 22;
		
		jeu[id] = new Affrontement(100,"Deck Beatdown","Deck Control","Deck Bait",id);
		jeu[id].descrition = "Sans commentaire… \n" + 
				"Malheureusement , un des amis de " + hero.prenom + " lui proposa un défi… “tant que tu ne me \n" + 
				"gagnes pas à clash royale tu refais une partie contre moi”. " + hero.prenom + " a un esprit \n" + 
				"tellement compétitif qu’il devait accepter le duel… Mais aussi le gagner en moins de cinq \n" + 
				"minutes ! Le premier round va bientôt commencer et il doit choisir un des ces meilleurs \n" + 
				"decks !\n" + 
				"";
		jeu[id].idSceneSuivante1 = 24;
/*		jeu[id].SceneSuivante1 = "";
		jeu[id].idSceneSuivante2 = ;
		jeu[id].SceneSuivante2 = "";
		jeu[id].idSceneSuivante3 = ;
		jeu[id].SceneSuivante3 = "";
*/		
		
		/*
		 * plus ou moins
		 */

		id = 23;
		
		jeu[id] = new PlusOuMoins(id);
		jeu[id].descrition = "Après un dîner avec Charlotte, spectaculaire cela va de soi, " + hero.prenom + " était en chemin \n" + 
				"pour rentrer chez lui. Malheureusement, ou pas en fait, il avait un peu trop engraissé le \n" + 
				"toboggan… Il devait à tout prix se rappeler de l'adresse de sa maison. Il vivait vers “ La _ \n" + 
				"avenue à Manhattan”.  \n" + 
				"";
		jeu[id].idSceneSuivante1 = 24;
/*		jeu[id].SceneSuivante1 = "";
		jeu[id].idSceneSuivante2 = ;
		jeu[id].SceneSuivante2 = "";
		jeu[id].idSceneSuivante3 = ;
		jeu[id].SceneSuivante3 = "";
		*/
		
		/*
		 * Maison
		 */

		id = 24;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = hero.prenom + " rentra chez lui. Bien plus tard, n’ayant pas envie de penser seulement à \n" + 
				"Charlotte, il décide de maintenir son esprit occupé. Il doit donc faire un choix. Soit il joue à \n" + 
				"super séducteur pour gagner en confiance en lui et améliorer son approche avec Charlotte. \n" + 
				"Soit il travaille pour s’assurer un avenir. Soit alors il pense carrément à Charlotte et donc \n" + 
				"l’appel. Il était déjà au quatrième niveau du jeu de super-séducteurs ! Il en avait eu quatre à \n" + 
				"son dernier examen d'économie… Et quelle coïncidence ! Le numéro de Charlotte finit par \n" + 
				"un 4 ! Il ne manquerait plus qu’il apparaisse une quatrième fois ! \n" + 
				"";
		jeu[id].idSceneSuivante1 = 25;
		jeu[id].SceneSuivante1 = "Joue à super seduceur";
		jeu[id].idSceneSuivante2 = 26;
		jeu[id].SceneSuivante2 = "Travailler";
		jeu[id].idSceneSuivante3 = 27;
		jeu[id].SceneSuivante3 = "Appeler Charlotte";
		
		
		
		
		/*
		 * Joue à super seduceur
		 */

		id = 25;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "En rentrant " + hero.prenom + " voulant garder son esprit occupé pour ne pas trop penser à \n" + 
				"Charlotte. Il se lance dans une partie de super seducer car il souhaite augmenter sa \n" + 
				"confiance en lui et ce jeu peut l’y aider. \n" + 
				"Tout à coup son téléphone vibra. Un message de Charlotte !!! Quel ne fut pas son \n" + 
				"étonnement! Etait-ce les bienfaits de super seducer ?!! \n" + 
				"Charlotte l’invitait à venir chez elle, pour une sympathique soirée pyjama. Mais grâce aux \n" + 
				"conseils du jeu, il ne se rua pas pour lui répondre. Il décida donc de la faire patienter et \n" + 
				"d'élaborer une réponse fun. \n" + 
				"";
		jeu[id].idSceneSuivante1 = 28;
		jeu[id].SceneSuivante1 = "Quand tu veux bébé";
		jeu[id].idSceneSuivante2 = 29;
		jeu[id].SceneSuivante2 = "Quand tu veux mon reuf";
		jeu[id].idSceneSuivante3 = 30;
		jeu[id].SceneSuivante3 = "Ok, à quelle heure chez toi ?";
		
		/*
		 * Travailler
		 */

		id = 26;
		
		String[] matiere = {"Math","SVT","Anglais","Physique","Philosophie"};
		
		jeu[id] = new Pendu(id,matiere);
		jeu[id].descrition = hero.prenom + " se munie donc de ses cahiers et commence à étudier avec acharnement. \n" + 
				"Quel matière travaillait t-il ? \n" + 
				"";
		jeu[id].idSceneSuivante1 = 31;
		jeu[id].SceneSuivante1 = "Mouais..";
		jeu[id].idSceneSuivante2 = 31;
		jeu[id].SceneSuivante2 = "Bon";
		jeu[id].idSceneSuivante3 = 31;
		jeu[id].SceneSuivante3 = "Voila voila ... ";
		
		/*
		 * Appeler Charlotte
		 */

		id = 27;
		
		jeu[id] = new Affrontement(100,"FC Barcelone","Real de madrid","Manchester united",id);
		jeu[id].descrition = "Ayant réussi à obtenir le numéro de Charlotte, il décide de l’appeler. Elle répondit \n" + 
				"rapidement. ils se mirent donc à parler et une connexion presque cosmique se lia entre eux. \n" + 
				"L’entente était à son comble, si bien que Charlotte lui proposa de venir chez elle pour une \n" + 
				"soirée pyjama. \n" + 
				"\n" + 
				"Arrivés chez elle, ils parlèrent un peu, se mirent en pyjama et commencèrent leur petite \n" + 
				"soirée sympathique.  Ils riaient, mangeaient, et même dansaient. Soudain, la conversation \n" + 
				"au grès du hasard dériva sur fifa, Charlotte lui assurait qu'elle était une pro et imbattable. Le \n" + 
				"défi était donc relevé et les voici qu’ils se mirent à jouer fifa dans les joies d’une séduction \n" + 
				"naissante. \n" + 
				"" + hero.prenom + " devait à tout prix gagner pour atteindre son cœur à tout jamais. \n" + 
				"";
		
		
		jeu[id].idSceneSuivante1 = Scene.IdMax;
/*		jeu[id].SceneSuivante1 = "";
		jeu[id].idSceneSuivante2 = ;
		jeu[id].SceneSuivante2 = "";
		jeu[id].idSceneSuivante3 = ;
		jeu[id].SceneSuivante3 = "";
*/		
		
		/*
		 * Quand tu veux bébé
		 */

		id = 28;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "Choquée par de tels propos, Charlotte s’offusqua : “Je te pensais galant et courtois… Mais \n" + 
				"je vois que tu n’es pas mieux que tous ces mecs qui prennent les filles pour des objets... \n" + 
				"Tant pis ! J’entends ma mère à l’oreillette qui me dit que finalement elle ne veut pas de \n" + 
				"garçons à la maison. Je suis désolée, une prochaine fois.” \n" + 
				"\n   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		/*
		 * Quand tu veux mon reuf
		 */

		id = 29;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "\n" + 
				"Charlotte est un peu étonnée derrière son téléphone. \"Ah Ah Ah, beh je demande à ma \n" + 
				"mère si elle est d’accord et je te tiens au courant.”\n" + 
				"Mais finalement Charlotte ne relança jamais " + hero.prenom + " et c’est ainsi que l'histoire se termine.\n" + 
				"\n   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		
		/*
		 * Ok, à quelle heure chez toi ?
		 */

		id = 30;
		
		jeu[id] = new PlusOuMoins(id);
		jeu[id].descrition = hero.prenom + " a décidé de la jouer fun mais un peu froid pour attirer Charlotte à lui, ce qui \n" + 
				"marcha. “Devine l’heure !” lui envoya-t-elle par message. \n" + 
				"";
		jeu[id].idSceneSuivante1 = 33;
/*		jeu[id].SceneSuivante1 = "";
		jeu[id].idSceneSuivante2 = ;
		jeu[id].SceneSuivante2 = "";
		jeu[id].idSceneSuivante3 = ;
		jeu[id].SceneSuivante3 = "";
*/		
		/*
		 * 
		 */

		id = 31;
		
		jeu[id] = new ParcoursSimple(id);
		jeu[id].descrition = "Son téléphone étant en mode avion, il ne vit pas le message de Charlotte. Et quand il le vit, \n" + 
				"il était trop tard ainsi, il ne put donc jamais sortir avec elle et resta avec ses chats se \n" + 
				"lamentant. \n" + 
				"\n   _____          __  __ ______    ______      ________ _____  \n" + 
				"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" + 
				" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" + 
				" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" + 
				" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" + 
				"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\\n" + 
				"                                                               \n" + 
				"                                                               ";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
		jeu[id].SceneSuivante1 = "FIN";
		jeu[id].idSceneSuivante2 = Scene.IdMax;
		jeu[id].SceneSuivante2 = "FIN";
		jeu[id].idSceneSuivante3 = Scene.IdMax;
		jeu[id].SceneSuivante3 = "FIN";
		
		/*
		 * FIFA
		 */

		id = 33;
		
		jeu[id] = new Affrontement(100,"FC Barcelone"," Real de madrid","Manchester united",id);
		jeu[id].descrition = "Arrivés chez elle, ils parlèrent un peu, se mirent en pyjama et commencèrent leur petite \n" + 
				"soirée sympathique.  Ils riaient, mangeaient, et même dansaient. Soudain, la conversation \n" + 
				"au grès du hasard dériva sur fifa, Charlotte lui assurait qu'elle était une pro et imbattable. Le \n" + 
				"défi était donc relevé et les voici qu’ils se mirent à jouer fifa dans les joies d’une séduction \n" + 
				"naissante. \n" + 
				"" + hero.prenom + " devait à tout prix gagner pour atteindre son cœur à tout jamais. \n" + 
				"";
		jeu[id].idSceneSuivante1 = Scene.IdMax;
/*		jeu[id].SceneSuivante1 = "";
		jeu[id].idSceneSuivante2 = ;
		jeu[id].SceneSuivante2 = "";
		jeu[id].idSceneSuivante3 = ;
		jeu[id].SceneSuivante3 = "";
	*/	

		return jeu;
	}
}
