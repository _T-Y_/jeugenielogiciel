import java.io.Serializable;

public class Equipement extends Outil implements Serializable{
	
	private static final long serialVersionUID = 1L;
	int valeurArmure;
	int valeurAttaque;
	
	Equipement(int id, String descriptionEquipement, int armure, int attaque){
		/** Cree un outil de type Equipement */
		this.id = id;
		this.bonusObjet = descriptionEquipement;
		this.valeurArmure = armure;
		this.valeurAttaque = attaque;
	}
}