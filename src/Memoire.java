import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Memoire {

	/***************************************************************************************************
										  __  __      _   _           
										 |  \/  |    | | (_)          
										 | \  / | ___| |_ _  ___ _ __ 
										 | |\/| |/ _ \ __| |/ _ \ '__|
										 | |  | |  __/ |_| |  __/ |   
										 |_|  |_|\___|\__|_|\___|_|   

	 ***************************************************************************************************/
	
	/*
	 * Sauvegarde le Personnage et le numéros de l'id de la scene en cours.
	 * Le personnage dipose d'un inventaire dans ces attributs, donc en sauvegardant
	 * le personnage, on sauvegarde également son inventaire.
	 */
	
      void enregistrement(Personnage heros, Inventaire sac, int idScene) throws FileNotFoundException, UnsupportedEncodingException {
    	  ObjectOutputStream oos;
    	  
    	  try{
	    	  oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("../sauvegarde/sauvegardePersonnage.txt"))));
	    	  oos.writeObject(heros);
	    	  oos.close();
    	  } catch (java.io.IOException e1) {
    		  e1.printStackTrace();
    	  }
    	  /*
    	  try{
	    	  oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("sauvegarde/sauvegardeInventaire.txt"))));
	    	  oos.writeObject(sac);
	    	  oos.close();
    	  } catch (java.io.IOException e1) {
    		  e1.printStackTrace();
    	  }
    	  */
    	  PrintWriter writer = new PrintWriter("../sauvegarde/sauvegardeScene.txt", "UTF-8");
    	  writer.print(idScene);
    	  writer.close();     	
      }
      
      public Personnage chargementPersonnage() throws IOException {
    	  Personnage hero = null;
    	  
    	  ObjectInputStream ois;
    	  
    	  //On récupère maintenant les données !
    	  ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File("../sauvegarde/sauvegardePersonnage.txt"))));

     	  try {
 	         hero = (Personnage) ois.readObject();
 	         System.out.println("\n\nAffichage Perso : " + hero.prenom);
 	         System.out.println("*************************\n");
 	       } catch (ClassNotFoundException e) {
 	         e.printStackTrace();
 	       }
 	 	
 	       ois.close();
 	       return hero;
      }
      /*
      public Inventaire chargementInventaire() throws FileNotFoundException, IOException {
    	  Inventaire sac = null;
    	  
    	  ObjectInputStream ois;
    	  
    	  //On récupère maintenant les données !
    	  ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File("sauvegarde/sauvegardeInventaire.txt"))));

     	  try {
 	         System.out.println("Affichage Inventaire :");
 	         System.out.println("*************************\n");
 	         //System.out.println(((Inventaire)ois.readObject()).toString());
 	         sac = (Inventaire) ois.readObject();
 	         //System.out.println(((Game)ois.readObject()).toString());
 	         //System.out.println(((Game)ois.readObject()).toString());
 	       } catch (ClassNotFoundException e) {
 	         e.printStackTrace();
 	       }
 	 	
 	       ois.close();
    	  
    	  return sac;
      }
      */
      public int chargementScene() throws IOException {
    	  int scene = 0;
    	  
    	  File fichier =new File("../sauvegarde/sauvegardeScene.txt");
          //Création du FileReader (voir doc)
          FileReader fileR = new FileReader(fichier);
          //Création du BufferedReader (voir doc)
          BufferedReader bufferedR = new BufferedReader(fileR);
          //Initilisation de l'objet String utilisé pour la lecture
          String s;

          
          //On lit chaque ligne jusqu'à ce qu'il n'y en ai plus
          s = bufferedR.readLine();
          
          //System.out.print("\n" + s + "\n");
    	  scene = Integer.valueOf(s);
          
          bufferedR.close();
          
          System.out.print("\nValuer de la scene charger : " + scene + "\n***********************\n\n");
          
    	  return scene;
      }
      
      /*
      public void chargement(int scene, Personnage hero, Inventaire sac) throws IOException {
    	  scene = this.chargementScene();
    	  sac = chargementInventaire();
    	  hero = chargementPersonnage();
      }
      */
      
      int play(int scene, Personnage hero, Inventaire sac) throws IOException {  	  
    	  int choix;
    	  
    	  this.afficheMemoire();
    	  choix = this.choixMemoire();
      
    	  if(choix == 0) {
    		  this.enregistrement(hero, sac, scene);
    	  }
    	  
    	  if(choix == 1) {
    		  Initialisation jeuInit;
    		  jeuInit = new Initialisation();
    		  
    		  hero = this.chargementPersonnage();
    		  scene = this.chargementScene();
    		  classeMain.setHero(hero);
    		  classeMain.initScript(jeuInit);
    		  classeMain.initIndiceScene(scene);
    	  }
    	  
    	  return choix;
      }
      
      /***************************************************************************************************
								  _____  _       _                        
								 |  __ \(_)     | |                       
								 | |  | |_  __ _| | ___   __ _ _   _  ___ 
								 | |  | | |/ _` | |/ _ \ / _` | | | |/ _ \
								 | |__| | | (_| | | (_) | (_| | |_| |  __/
								 |_____/|_|\__,_|_|\___/ \__, |\__,_|\___|
								                          __/ |           
								                         |___/            

       ***************************************************************************************************/
      
      void afficheMemoire() {
    	  System.out.print("\nSauvegarder ? (0) / Charger ? (1)\n");
      }
      
      /***************************************************************************************************
									
								   _____            _        //\  _      
								  / ____|          | |      |/ \|| |     
								 | |     ___  _ __ | |_ _ __ ___ | | ___ 
								 | |    / _ \| '_ \| __| '__/ _ \| |/ _ \
								 | |___| (_) | | | | |_| | | (_) | |  __/
								  \_____\___/|_| |_|\__|_|  \___/|_|\___|
							          

       ***************************************************************************************************/

      int choixMemoire() {
  		int choix = 0;
  		
  		System.out.print("Votre choix : ");
  		
  		Scanner sc = new Scanner(System.in);
  		choix = sc.nextInt();
  		
  		if(choix == 0 || choix == 1)
  			return choix;
  		
  		System.out.print("Choix non valide\n");
  		return this.choixMemoire();
  	}   
    
}
